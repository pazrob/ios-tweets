//
//  TableView+Ext.swift
//  Tweets
//
//  Created by Robinson Paz Jesus on 9/26/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import UIKit

extension UITableView {
    
    static func getTweetsUITableView(container: UIView) -> UITableView {
        let tableView = UITableView()
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableView.automaticDimension
        tableView.contentInset = UIEdgeInsets(top: 32, left: 0, bottom: 32, right: 0)
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        container.addSubview(tableView)
        tableView.fillSuperview()
        return tableView
    }
}


