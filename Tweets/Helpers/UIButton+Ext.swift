//
//  UIButton+Ext.swift
//  Bacon
//
//  Created by Robinson Paz Jesus on 9/5/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import UIKit

extension UIButton {
    
    convenience init(title: String) {
        self.init(type: .system)
        self.setTitle(title, for: .normal)
    }
    
    func setStyle(on: Bool) {
        self.isSelected = on
        if on {
            setTitle("Follow", for: .normal)
            backgroundColor = Color.primary.value
        } else {
            setTitle("Unfollow", for: .normal)
            backgroundColor = UIColor.lightGray
        }
    }
    
    func setEnable(with isEnabled: Bool) {
        
        if isEnabled {
            self.backgroundColor = Color.primary.value
            self.isEnabled = true
        } else {
            self.backgroundColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 1)
            self.isEnabled = false
        }
    }
}
