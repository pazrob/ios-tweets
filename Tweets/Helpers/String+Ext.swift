//
//  String+Ext.swift
//  Bacon
//
//  Created by Robinson Paz Jesus on 9/5/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import Foundation


extension String {
    
    var digits: String {
        return components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
    }
    
    static func isValidEmail(email:String?) -> Bool {
        guard let emailIn = email else { return false }
        //contains @, a dot after, two chars after,
        let regEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}(\\s){0,1}"
        let pred = NSPredicate(format:"SELF MATCHES %@", regEx)
        return pred.evaluate(with: emailIn)
    }
    
    static func isValidHanle(handle: String?) -> Bool {
        guard let handle = handle else { return false }
        if handle.isEmpty { return false }
        if !handle.hasPrefix("@") { return false }
        if handle.count < 2 { return false}
        return true
    }
    
    static func isValidPassword(password:String?) -> Bool {
        guard let password = password else { return false }
        if password.isEmpty { return false }
        if password.count < 4 { return false} 
        return true
    }
    
    static func isValidNaming(check: [String?]) -> Bool {
        for name in check {
            guard let nameString = name else { return false }   //check exists
            if nameString.isEmpty { return false }              //check if empty
            if nameString.count < 1 { return false }            //check if less than 2
        }
        return true
    }
}

extension StringProtocol {
    var double: Double? {
        return Double(self)
    }
    var float: Float? {
        return Float(self)
    }
    var integer: Int? {
        return Int(self)
    }
}
