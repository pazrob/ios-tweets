//
//  ActivityIndicator.swift
//  Bacon
//
//  Created by Robinson Paz Jesus on 2/21/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import UIKit

extension UIAlertController {
    
    static func presentSheet(viewController: UIViewController,
                             message: String? = nil,
                             actions: [UIAlertAction]) {
        
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        for action in actions {
            alert.addAction(action)
        }
        alert.addAction(cancel)
        
        viewController.present(alert, animated: true, completion: nil)
    }
    
    static func presentError(viewController: UIViewController,
                             title: String? = "ERROR",
                             message: String? = "There has been an error.") {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(ok)
        viewController.present(alert, animated: true, completion: nil)
    }

    

    
    
}
