//
//  AWSMain.swift
//  Tweets
//
//  Created by Robinson Paz Jesus on 10/28/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import Foundation
import AWSCore
import AWSS3
import AWSCognitoIdentityProvider

let AWSCognitoUserPoolsSignInProviderKey = "UserPool"
let pool = AWSCognitoIdentityUserPool(forKey: AWSCognitoUserPoolsSignInProviderKey)

class AWSMain {
    
    static var shared = AWSMain()
    private init() {}
    
    func defaultUserPool() -> AWSCognitoIdentityUserPool {
        return AWSCognitoIdentityUserPool(forKey: CognitoIdentityUserPoolId)
    }
    
    let CognitoIdentityUserPoolRegion: AWSRegionType = .EUWest2
    let CognitoIdentityUserPoolId = "us-west-2_zo6w4x0lp"
    let CognitoIdentityUserPoolAppClientId = "7ff1bec4sgkgvsj0cjjqokc5am"
    let CognitoIdentityUserPoolAppClientSecret = "vdm9bmtmpuio51fka75cvpvajjajhhfd99csaj4g03o4vfuj4l6"

    func start() {
        
        // Amazon for s3? Cognito
        let accessKey = "AKIAIDWUFJQ6YB3ANPBA"
        let secretKey = "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"
        let credentialsProvider = AWSStaticCredentialsProvider(accessKey: accessKey, secretKey: secretKey)
        let serviceConfiguration = AWSServiceConfiguration(region: .USWest2, credentialsProvider: credentialsProvider)
        AWSServiceManager.default().defaultServiceConfiguration = serviceConfiguration
        
        // Amazon cognito
        let poolConfiguration = AWSCognitoIdentityUserPoolConfiguration(clientId: CognitoIdentityUserPoolAppClientId,
                                                                        clientSecret: CognitoIdentityUserPoolAppClientSecret,
                                                                        poolId: CognitoIdentityUserPoolId)
        AWSCognitoIdentityUserPool.register(with: serviceConfiguration,
                                            userPoolConfiguration: poolConfiguration,
                                            forKey: AWSCognitoUserPoolsSignInProviderKey)
        
    }
}
