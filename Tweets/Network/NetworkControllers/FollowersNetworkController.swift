//
//  FollowersNetworkController.swift
//  Tweets
//
//  Created by Robinson Paz Jesus on 10/17/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import PromiseKit

class FollowersNetworkController {
    
    let followersService = FollowersNetworkService(networkProxy: NetworkProxy.shared)
    
    func startFollowing(user: User) -> Promise<EmptyResponse> {
        return followersService.startFollowing(user: user)
    }
    
    func stopFollowing(handle: String) -> Promise<EmptyResponse> {
        return followersService.stopFollowing(handle: handle)
    }
    
    func isFollowingRelation(handleUser: String, handleFollowing: String) -> Promise<Bool> {
        return followersService.isFollowingRelation(handleUser: handleUser, handleFollowing: handleFollowing)
    }
    
    /// Users that follow the user
    /// - Parameter handle: username
    func getFollowers(for handle: String, reloadData: Bool = false, sortKey: String?) -> Promise<UserListResponse> {
        return followersService.getFollowers(for: handle, followedByKey: sortKey)
    }
    
    
    /// Users who the user follows
    /// - Parameter handle: username
    func getFollowing(for handle: String, reloadData: Bool = false, sortKey: String?) -> Promise<UserListResponse> {
        return followersService.getFollowing(for: handle, followingKey: sortKey)
    }
    
}
