//
//  TestNetworkController.swift
//  Tweets
//
//  Created by Robinson Paz Jesus on 9/30/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import Foundation
import PromiseKit

class UserNetworkController {
    private let userServicer = UserNetworkService(networkProxy: NetworkProxy.shared)
    
    func register(firstName: String, lastName: String, handle: String, password: String, completed: @escaping (Bool, String?)->Void) {
        return userServicer.register(firstName: firstName, lastName: lastName, handle: handle, password: password, completed: completed)
    }
    
    func login(handle: String, password: String, completed: @escaping (Bool, String?)->Void) {
        return userServicer.login(handle: handle, password: password, completed: completed)
    }
    
    func searchUser(handle: String) -> Promise<User> {
        return userServicer.searchUser(handle: handle)
    }
    
}
