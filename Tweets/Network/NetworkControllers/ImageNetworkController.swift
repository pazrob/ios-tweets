//
//  ImageNetworkController.swift
//  Tweets
//
//  Created by Robinson Paz Jesus on 10/3/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import UIKit

class ImageNetworkController {
    
    let imageService = ImageNetorkService(networkProxy: NetworkProxy.shared)
    
    func updateUserPhoto(originalUrl: URL, image: UIImage, callback: @escaping (UIImage?) -> Void) {
        return imageService.updateUserPhoto(originalUrl: originalUrl, image: image) { image in
            TweetsSession.shared.photo = image
            callback(image)
        }
    }
    
    func uploadImage(originalUrl: URL, image: UIImage?, isUserProfile: Bool, callback: @escaping ((URL?)->Void)) {
        return imageService.uploadImage(originalUrl: originalUrl, image: image, isUserProfile: isUserProfile, callback: callback)
    }
    
    func uploadVideo(fileUrl : URL, callback: @escaping ((URL?)->Void)) {
        imageService.uploadVideo(fileUrl: fileUrl, callback: callback)
    }
}
