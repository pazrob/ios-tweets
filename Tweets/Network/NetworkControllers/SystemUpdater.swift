//
//  MainNetworkUpdater.swift
//  Tweets
//
//  Created by Robinson Paz Jesus on 10/15/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import Foundation

protocol SystemUpdaterDelegateContent {
    func systemUpdater(forFollowees: [User], isNewPage: Bool, lastKey: String?)
    func systemUpdater(forFollowers: [User], isNewPage: Bool, lastKey: String?)
    func systemUpdater(forStory: [Status], isNewPage: Bool, lastKey: String?)
    func systemUpdater(forFeed: [Status], isNewPage: Bool, lastKey: String?)
}

protocol SystemUpdaterDelegateUser {
    func systemUpdater(forUser: User)
}

class SystemUpdater {
    
    var delegateUser: SystemUpdaterDelegateUser?
    var delegateContent: SystemUpdaterDelegateContent?
    
    func fetchUser(handle: String, completed: @escaping ()->Void) {
        
        let userServicer = UserNetworkService(networkProxy: NetworkProxy.shared)
        userServicer.getUser(handle: handle)
            .done { user in
                self.delegateUser?.systemUpdater(forUser: user)
                print(" SYSTEM UPDATER =======>> Done: Get user")
        }.catch { error in
            log.error(error, context: "GET USER")
            return
        }.finally {
            completed()
        }
    }
    
    func fetchContent(handle: String, completed: @escaping ()->Void) {
        
        let statusServicer = StatusNetworkService(networkProxy: NetworkProxy.shared)
        let followingServicer = FollowersNetworkService(networkProxy: NetworkProxy.shared)
        
        let dispatchGroup = DispatchGroup()
        
        
        print(" SYSTEM UPDATER <<======= Starting: Get story")
        dispatchGroup.enter()
        statusServicer.getStory(for: handle, sortKey: nil)
            .done { statusResponse in
                self.delegateContent?.systemUpdater(forStory: statusResponse.items!,
                                                    isNewPage: false,
                                                    lastKey: statusResponse.lastEvaluatedKey?.sortKey)
                print(" SYSTEM UPDATER =======>> Done: Get story")
        }.catch { error in
            log.error(error, context: "GET STORY")
        }.finally {
            dispatchGroup.leave()
        }
        
        dispatchGroup.enter()
        print(" SYSTEM UPDATER <<======= Starting: Get following")
        followingServicer.getFollowing(for: handle, followingKey: nil)
            .done { listResponse in
                self.delegateContent?.systemUpdater(forFollowees: listResponse.items!,
                                                    isNewPage: false,
                                                    lastKey: listResponse.lastEvaluatedKey?.sortKey)
                print(" SYSTEM UPDATER =======>> Done: Get following")
        }.catch { error in
            log.error(error, context: "GET FOLLOWEES")
        }.finally {
            dispatchGroup.leave()
        }
        
        dispatchGroup.enter()
        print(" SYSTEM UPDATER <<======= Starting: Get followers")
        followingServicer.getFollowers(for: handle, followedByKey: nil)
            .done { listResponse in
                self.delegateContent?.systemUpdater(forFollowers: listResponse.items!,
                                                    isNewPage: false,
                                                    lastKey: listResponse.lastEvaluatedKey?.sortKey)
                print(" SYSTEM UPDATER =======>> Done: Get followers")
        }.catch { error in
            log.error(error, context: "GET FOLLOWERS")
        }.finally {
            dispatchGroup.leave()
        }
        
        dispatchGroup.notify(queue: .main) {
            completed()
        }
    }
    
    /// This is called after a user is authenticated.
    func updateAll(with handle: String?) {
        
        guard let handle = handle else {
            log.error("The handle was not passed properly.", context: "HANDLE EMPTY")
            return
        }
        
        let dispatchQueue = DispatchQueue.global(qos: .background)
        let semaphore = DispatchSemaphore(value: 0)
        
        print("============== SYSTEM UPDATER START ==============")
        print(" SYSTEM UPDATER <<======= Starting: Get user")
        
        dispatchQueue.async {
            // Get user first using semaphores
            self.fetchUser(handle: handle) {
                semaphore.signal()
            }
            semaphore.wait()
            
            // Once semaphore is 'green' fetch the rest.
            self.fetchContent(handle: handle) {
                
            }
        }
    }
    
    func fetchMoreFeed(handle: String, sortKey: String?, completed: @escaping ()->Void) {
        let statusNetwork = StatusNetworkController()
        statusNetwork.getFeed(for: handle, sortKey: sortKey)
            .done { feedPageResponse in
                self.delegateContent?.systemUpdater(forFeed: feedPageResponse.items!,
                                                    isNewPage: sortKey != nil,
                                                    lastKey: feedPageResponse.lastEvaluatedKey?.sortKey)
        }.catch { error in
            log.error(error)
        }.finally {
            completed()
        }
    }
    
    func fetchMoreStory(handle: String, sortKey: String?, completed: @escaping ()->Void) {
        let statusNetwork = StatusNetworkController()
        statusNetwork.getStory(for: handle, sortKey: sortKey)
            .done { storyPageResponse in
                self.delegateContent?.systemUpdater(forStory: storyPageResponse.items!,
                                                    isNewPage: sortKey != nil,
                                                    lastKey: storyPageResponse.lastEvaluatedKey?.sortKey)
        }.catch { error in
            log.error(error)
        }.finally {
            completed()
        }
    }
    
    func fetchMoreFollowing(handle: String, sortKey: String?, completed: @escaping ()->Void) {
        let followNetwork = FollowersNetworkController()
        followNetwork.getFollowing(for: handle, sortKey: sortKey)
            .done { pageResponse in
                self.delegateContent?.systemUpdater(forFollowees: pageResponse.items!,
                                                    isNewPage: sortKey != nil,
                                                    lastKey: pageResponse.lastEvaluatedKey?.sortKey)
        }.catch { error in
            log.error(error)
        }.finally {
            completed()
        }
    }
    
    func fetchMoreFollowers(handle: String, sortKey: String?, completed: @escaping ()->Void) {
        let followNetwork = FollowersNetworkController()
        followNetwork.getFollowers(for: handle, sortKey: sortKey)
            .done { pageResponse in
                self.delegateContent?.systemUpdater(forFollowers: pageResponse.items!,
                                                    isNewPage: sortKey != nil,
                                                    lastKey: pageResponse.lastEvaluatedKey?.sortKey)
        }.catch { error in
            log.error(error)
        }.finally {
            completed()
        }
    }
}
