//
//  StatusNetworkController.swift
//  Tweets
//
//  Created by Robinson Paz Jesus on 10/1/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import Foundation
import PromiseKit

class StatusNetworkController {
    
    private let statusServicer = StatusNetworkService(networkProxy: NetworkProxy.shared)
    
    func postStatus(text: String, s3URL: URL?) -> Promise<Status> {
        return statusServicer.postStatus(text: text, s3URL: s3URL)
    }
    
    func getFeed(for handle: String, sortKey: String?) -> Promise<StatusListResponse> {
        return statusServicer.getFeed(for: handle, sortKey: sortKey)
    }
    
    func getStory(for handle: String, sortKey: String?) -> Promise<StatusListResponse> {
        return statusServicer.getStory(for: handle, sortKey: sortKey)
    }
    
    func getStatuses(for hashtag: String) -> Promise<StatusListResponse> {
        return statusServicer.getStatuses(for: hashtag)
    }
}
