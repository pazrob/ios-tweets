//
//  Encoder+Ext.swift
//  Bacon
//
//  Created by Andre Carrera on 2/1/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import Foundation


/// This...
protocol EncodeDecode {
    func encode (data: Codable) throws -> Data
    func decode<T>(_ type: T.Type, from data: Data) throws -> T where T : Decodable
}

//encode and decode
extension NetworkProxy: EncodeDecode {
    func decode <T>(_ type: T.Type, from data: Data) throws -> T where T : Decodable {
        return try decoder.decode(type, from: data)
    }
    
    func encode (data: Codable) throws -> Data {
        let resultDictionary = try data.encode(with: self.encoder)
        return resultDictionary
    }
}


enum encodeError: Error {
    case error
}


extension Encodable {
    func encode(with encoder: JSONEncoder = JSONEncoder()) throws -> Data {
        return try encoder.encode(self)
    }
}

extension Decodable {
    static func decode(with decoder: JSONDecoder = JSONDecoder(), from data: Data) throws -> Self {
        return try decoder.decode(Self.self, from: data)
    }
}

extension Encodable {
    func asDictionary(with encoder: JSONEncoder = JSONEncoder()) throws -> JSON {
        let data = try encoder.encode(self)
        guard let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
            throw encodeError.error
        }
        return dictionary
    }
}

extension Data {
    func toString() -> String{
        let dataString = String(data: self, encoding: String.Encoding.utf8)
        return dataString ?? ""
    }
}
