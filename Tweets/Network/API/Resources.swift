//
//  Resources.swift
//  Tweets
//
//  Created by Robinson Paz Jesus on 9/30/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import Foundation
import Alamofire

public typealias AuthString = String

public enum Resource {
    // User
    case getUser(String)
    case updatePhotoURL
    
    //Follow
    case startFollowing
    case stopFollowing
    case isFollowing(String, String)
    case getFollowing(String, String?)
    case getFollowers(String, String?)
    
    //Status
    case createPost
    case getHashtagStatus(String) // hashtag
    case getFeed(String, String?)
    case getStory(String, String?)
    
    
    public var resource: (method: HTTPMethod, route: String, needsAuth: Bool) {
        switch self {
            
        case .getUser(let handle):
            return (.get, "/users/\(handle)", false)
        case .updatePhotoURL:
            return (.post, "/users/update-url", true)
            
        case .startFollowing:
            return (.post, "/following/add", true)
        case .stopFollowing:
            return (.delete, "/following/delete", true)
        case .isFollowing(let isUser, let followingHandle):
            return (.get, "/following/connection/?follower=\(isUser)&following=\(followingHandle)", false)
        case .getFollowing(let handle, let followingSortkey):
            if let key = followingSortkey {
                return (.get, "/following/\(handle)?following=\(key)", false)
            } else {
                return (.get, "/following/\(handle)", false)
            }
        case .getFollowers(let handle, let followedByKey):
            if let key = followedByKey {
                return (.get, "/followers/\(handle)?followedBy=\(key)", false)
            } else {
                return (.get, "/followers/\(handle)", false)
            }
            
        case .createPost:
            return (.post, "/status", true)
        case .getHashtagStatus(let hashtag):
            return (.get, "/status/\(hashtag.replacingOccurrences(of: "#", with: ""))", false)
        case .getFeed(let handle, let timestamp):
            if let timestamp = timestamp {
                return (.get, "/status/feed/\(handle)?timestamp=\(timestamp)", false)
            } else {
                return (.get, "/status/feed/\(handle)", false)
            }
        case .getStory(let handle, let timestamp):
            if let timestamp = timestamp {
                return (.get, "/status/story/\(handle)?timestamp=\(timestamp)", false)
            } else {
                return (.get, "/status/story/\(handle)", false)
            }
        }
    }
}
