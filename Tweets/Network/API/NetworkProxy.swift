//
//  NetworkProxy.swift
//  Tweets
//
//  Created by Robinson Paz Jesus on 9/30/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit
import KeychainAccess

public typealias JSON = [String: Any]

public enum ServerURL: String {
    case aws = "https://5psvnfp1qh.execute-api.us-west-1.amazonaws.com/dev"
}

class NetworkProxy {
    
    static let shared = NetworkProxy(baseURL: ServerURL.aws)
    var baseURL: String
    var defaultHeader: HTTPHeaders = [ "Content-Type": "application/json"]
    let decoder = JSONDecoder()
    let encoder = JSONEncoder()
    
    let keychain = Keychain(service: "com.Tweets")
    
    private init(baseURL: ServerURL = .aws) {
        self.baseURL = baseURL.rawValue
        self.decoder.keyDecodingStrategy = .convertFromSnakeCase
        encoder.outputFormatting = [.prettyPrinted, .sortedKeys]
        encoder.keyEncodingStrategy = .convertToSnakeCase
    }
    
    public func request(_ resource: Resource, parameters: [String:Any] = [:], headers: HTTPHeaders = [:]) -> Promise<Data> {
        return Promise { seal in
            let method = resource.resource.method
            let url = "\(baseURL)\(resource.resource.route)"
            log.info(parameters, "PARAMETERS")
            
            Alamofire.request(url,
                              method: method,
                              parameters: method == .get ? nil : parameters,
                              encoding: JSONEncoding.default,
                              headers: headers)
                .validate().responseData { response in
                    switch response.result {
                        
                    case .success(let json):
                        seal.fulfill(json)
                        
                    case .failure(let error):
                        seal.reject(error)
                    }
            }
        }
    }
    
    func simpleRequest(_ resource: Resource, parameters: Codable? = nil)-> Promise<(Data, URLResponse)> {
        return Promise { seal in
            do {
                let url = try "\(baseURL)\(resource.resource.route)".asURL()
                let urlRequest = createUrlRequest(url: url, body: parameters, resource: resource)
                
                URLSession.shared.dataTask(.promise, with: urlRequest).validate()
                    .map { args in
                        log.debug(args.data.toString(), "Response")
                        seal.fulfill(args)
                    }.catch {
                        seal.reject($0)
                }
            }
            catch {
                seal.reject(error)
            }
        }
    }
    
    func codableRequest<T>(_ resource: Resource, parameters: Codable? = nil, as type: T.Type) -> Promise<T> where T : Decodable {
        return Promise { seal in
            log.info(resource.resource.route, "Route")
            do {
                let url = try "\(baseURL)\(resource.resource.route)".asURL()
                let urlRequest = createUrlRequest(url: url, body: parameters, resource: resource)
                URLSession.shared.dataTask(.promise, with: urlRequest).validate()
                    .map { args in
                        log.debug(args.data.toString(), "Response")
                        let decoded = try self.decode(type, from: args.data)
                        seal.fulfill(decoded)
                }
                .catch { error in
                    seal.reject(error)
                }
            }
            catch {
                seal.reject(error)
            }
        }
    }
    
    func createUrlRequest(url: URL, body: Codable?, resource: Resource) -> URLRequest {
        var rq = URLRequest(url: url)
        rq.httpMethod = resouceToMethod(resource: resource)
        rq.addValue("application/json", forHTTPHeaderField: "Content-Type")
        rq.addValue("application/json", forHTTPHeaderField: "Accept")
        if resource.resource.needsAuth, let idToken = TweetsSession.shared.idToken {
            rq.addValue(idToken, forHTTPHeaderField: "Authorization")
        }
        if let codable = body, let params = try? self.encode(data: codable){
            log.debug(params.toString(), "Request")
            rq.httpBody = params
        }
        return rq
    }
    
    func resouceToMethod(resource: Resource) -> String {
        switch resource.resource.method {
        case .post:
            return "POST"
        case .get:
            return "GET"
        case .put:
            return "PUT"
        case .delete:
            return "DELETE"
        case .patch:
            return "PATCH"
        default:
            return "GET"
        }
    }
    
}

extension NetworkProxy {
    func setAuthToken(authToken: String) throws {
        try self.keychain.set(authToken, key: "authToken")
    }
    
    func getAuthToken() -> String? {
        return keychain["authToken"]
    }
    
    func removeAuthToken() throws {
        try self.keychain.remove("authToken")
    }
}
