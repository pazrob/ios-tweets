//
//  FollowersNetworkService.swift
//  Tweets
//
//  Created by Robinson Paz Jesus on 10/15/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import Foundation
import PromiseKit
import Alamofire

class FollowersNetworkService: NetworkService {
    
    func startFollowing(user: User) -> Promise<EmptyResponse> {
        firstly { () -> Promise<EmptyResponse> in
            let parameters = [
                "authorHandle": TweetsSession.shared.user!.handle!,
                "followHandle": user.handle!,
                "info": [
                    "handle": user.handle!,
                    "firstName": user.firstName,
                    "lastName": user.lastName,
                    "photoUrl": user.photoUrl
                ],
                "author": [
                    "handle": TweetsSession.shared.user!.handle!,
                    "firstName": TweetsSession.shared.user!.firstName,
                    "lastName": TweetsSession.shared.user!.lastName,
                    "photoUrl": TweetsSession.shared.user!.photoUrl!
                ] as [String : Any]
            ] as [String : Any]
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
//                "Authorization":  TweetsSession.shared.idToken!
            ]
            return networkProxy.request(.startFollowing, parameters: parameters, headers: headers)
            .map { data in
                let decoder = JSONDecoder()
                return try decoder.decode(EmptyResponse.self, from: data)
            }
        }
    }
    
    func stopFollowing(handle: String) -> Promise<EmptyResponse> {
        firstly { () -> Promise<EmptyResponse> in
            let parameters = [
            "myself": TweetsSession.shared.user!.handle!,
            "unfollow": handle
            ]
            return networkProxy.codableRequest(.stopFollowing, parameters: parameters, as: EmptyResponse.self)
        }
    }
    
    func isFollowingRelation(handleUser: String, handleFollowing: String) -> Promise<Bool> {
        return networkProxy.codableRequest(.isFollowing(handleUser, handleFollowing), parameters: nil, as: Bool.self)
    }
    
    /// Users that follow the user
    /// - Parameter handle: username
    func getFollowers(for handle: String, followedByKey: String?) -> Promise<UserListResponse> { // 20 page
        return networkProxy.codableRequest(.getFollowers(handle, followedByKey), parameters: nil, as: UserListResponse.self)
    }
    
    
    /// Users who the user follows
    /// - Parameter handle: username
    func getFollowing(for handle: String, followingKey: String?) -> Promise<UserListResponse> { // 20 page
        return networkProxy.codableRequest(.getFollowing(handle, followingKey), parameters: nil, as: UserListResponse.self)
    }
}
