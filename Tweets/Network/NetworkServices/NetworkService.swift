//
//  NetworkService.swift
//  Tweets
//
//  Created by Robinson Paz Jesus on 10/1/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import Foundation

class NetworkService {
    let networkProxy: NetworkProxy
    
    init(networkProxy: NetworkProxy) {
        self.networkProxy = networkProxy
    }
    
}
