//
//  TestNetworkService.swift
//  Tweets
//
//  Created by Robinson Paz Jesus on 9/30/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import Foundation
import PromiseKit
import AWSCognitoIdentityProvider

class UserNetworkService: NetworkService {
    
    let pool = AWSCognitoIdentityUserPool(forKey: AWSCognitoUserPoolsSignInProviderKey)
    
    func register(firstName: String, lastName: String, handle: String, password: String, completed: @escaping (Bool, String?)->Void) {
        
        // Setting attributes
        var attributes = [AWSCognitoIdentityUserAttributeType]()
        
        if let firstNameAttribute = AWSCognitoIdentityUserAttributeType() {
            firstNameAttribute.name = "custom:first_name"
            firstNameAttribute.value = firstName
            attributes.append(firstNameAttribute)
        }
        
        if let lastNameAttribute = AWSCognitoIdentityUserAttributeType() {
            lastNameAttribute.name = "custom:last_name"
            lastNameAttribute.value = lastName
            attributes.append(lastNameAttribute)
        }
        
        if let handleAttribute = AWSCognitoIdentityUserAttributeType() {
            handleAttribute.name = "custom:handle"
            handleAttribute.value = handle
            attributes.append(handleAttribute)
        }
        
        pool.signUp(handle, password: password, userAttributes: attributes, validationData: nil).continueWith { task -> Any? in
            DispatchQueue.main.async(execute: {
                if let error = task.error as NSError? {
                    log.error(error)
                    completed(false, error.userInfo["message"] as? String)
                } else if let cognitoUser = task.result?.user {
                    cognitoUser.getSession().continueWith { session in
                        let newUser = User(firstName: firstName, lastName: lastName, handle: handle, photoUrl: nil)
                        TweetsSession.shared.updateUser(user: newUser)
                        TweetsSession.shared.handle = handle
                        completed(true, nil)
                        return nil
                    }
                }
            })
            return nil
        }
    }
        
    func login(handle: String, password: String, completed: @escaping (Bool, String?)->Void) {
            let user = pool.getUser(handle)
            user.getSession(handle, password: password, validationData: nil).continueWith { (task) -> Any? in
                DispatchQueue.main.async(execute: {
                    if let error = task.error as NSError? {
                        log.error(error)
                        completed(false, error.userInfo["message"] as? String)
                    } else if let session = task.result {
                        TweetsSession.shared.updateSession(session: session)
                        TweetsSession.shared.handle = handle
                        completed(true, nil)
                    }
                })
                return nil
        }
    }
    
    func getUser(handle: String) -> Promise<User> {
        return firstly { () -> Promise<User> in
            return networkProxy.codableRequest(.getUser(handle), parameters: nil, as: User.self)
        }
    }
    
    func searchUser(handle: String) -> Promise<User> {
        return firstly { () -> Promise<User> in
            return networkProxy.codableRequest(.getUser(handle), as: User.self)
        }
    }
}

enum NetworkError: Error {
    case decodeError
    case encodeError
    case authTokenError
    case imageError
}
