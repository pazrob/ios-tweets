//
//  ImageNetworkService.swift
//  Tweets
//
//  Created by Robinson Paz Jesus on 10/17/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import AWSS3
import Photos
import PromiseKit

class ImageNetorkService: NetworkService {
    
    // Permissions
    let S3BucketName = "tweets-bucket-cs340"
    let imageContentType = "image/jpeg"
    let videoContentType = "movie/mov"
    let userFolder = "profile/"
    let videoFolder = "video/"
    let imageFolder = "image/"
    
    func updateUserPhoto(originalUrl: URL, image: UIImage, callback: @escaping (UIImage?) -> Void) {
        
        // Semaphore
        let semaphore = DispatchSemaphore(value: 0)
        let dispatchQueue = DispatchQueue.global(qos: .background)
        var s3Url: URL?
        
        dispatchQueue.async {
            // >>> Send file to s3
            self.uploadImage(originalUrl: originalUrl, image: image, isUserProfile: true) { url in
                s3Url = url
                semaphore.signal()
            }
            semaphore.wait()
            
            // >>> Upload photo url
            self.updateUserURL(url: s3Url)
                .done { _ in
                    callback(image)
            }.catch { error in
                log.error(error)
                callback(nil)
            }
        }
    }
    
    func uploadImage(originalUrl: URL, image: UIImage?, isUserProfile: Bool, callback: @escaping ((URL?)->Void)) {
        guard let user = TweetsSession.shared.user, let uploadRequest =  AWSS3TransferManagerUploadRequest() else {
            callback(nil)
            return
        }
        
        // Prepare request
        uploadRequest.body = originalUrl//getImageUrl(url: originalUrl, image: image, resolution: 0.1)
        uploadRequest.key = isUserProfile ? "\(user.handle!)_\(UUID().uuidString)" :  UUID().uuidString
        uploadRequest.bucket = S3BucketName
        uploadRequest.contentType = imageContentType
        uploadRequest.acl = .publicRead
        
        // Send
        let transferManager = AWSS3TransferManager.default()
        transferManager.upload(uploadRequest).continueWith(executor: AWSExecutor.mainThread(), block: { task in
            var finalUrl: URL?
            if let error = task.error {
                log.error(error)
            } else {
                let baseAWSUrl = AWSS3.default().configuration.endpoint.url
                finalUrl = baseAWSUrl?.appendingPathComponent(uploadRequest.bucket!).appendingPathComponent(uploadRequest.key!)
            }
            callback(finalUrl)
            return nil
        })
    }
    
    func uploadVideo(fileUrl : URL, callback: @escaping ((URL?)->Void)) {
        guard let uploadRequest =  AWSS3TransferManagerUploadRequest() else {
            callback(nil)
            return
        }
        
        let newKey = "\(videoFolder)/\(UUID().uuidString).mov"
        uploadRequest.body = fileUrl as URL
        uploadRequest.key = newKey
        uploadRequest.bucket = S3BucketName
        uploadRequest.acl = .publicRead
        uploadRequest.contentType = videoContentType
        
        uploadRequest.uploadProgress = { (bytesSent, totalBytesSent, totalBytesExpectedToSend) -> Void in
            DispatchQueue.main.async(execute: {
                let amountUploaded = totalBytesSent // To show the updating data status in label.
                print(amountUploaded)
            })
        }
        
        let transferManager = AWSS3TransferManager.default()
        transferManager.upload(uploadRequest).continueWith(executor: AWSExecutor.mainThread(), block: { task in
            var finalUrl: URL?
            if let error = task.error {
                log.error(error)
            } else {
                let baseAWSUrl = AWSS3.default().configuration.endpoint.url
                finalUrl = baseAWSUrl?.appendingPathComponent(uploadRequest.bucket!).appendingPathComponent(uploadRequest.key!)
            }
            callback(finalUrl)
            return nil
        })
        
    }
    
    private func updateUserURL(url: URL?) -> Promise<EmptyResponse> {
        guard let url = url else { return Promise.init(error: NetworkError.imageError) }
        let params = [
            "handle": TweetsSession.shared.handle,
            "url": url.absoluteString
        ]
        return networkProxy.codableRequest(.updatePhotoURL, parameters: params, as: EmptyResponse.self)
    }
    
    private func getImageUrl(url: URL, image: UIImage, resolution: CGFloat) -> URL {
        let fileURL = url
        let data = image.jpegData(compressionQuality: resolution)
        do {
            try data?.write(to: fileURL)
        }
        catch {}
        return fileURL
    }
}
