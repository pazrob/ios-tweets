//
//  StatusNetworkService.swift
//  Tweets
//
//  Created by Robinson Paz Jesus on 10/1/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import Foundation
import PromiseKit
import Alamofire

class StatusNetworkService: NetworkService {
    
    func postStatus(text: String, s3URL: URL?) -> Promise<Status> {
        firstly { () -> Promise<Status> in
            var parameters = [
                "id": "TW_" + UUID().uuidString,
                "timestamp": Int(Date().timeIntervalSince1970),
                "text": text.replacingOccurrences(of: "\n", with: "\\n "),
                "handle": TweetsSession.shared.handle!,
                "fullName": "\(TweetsSession.shared.user.firstName) \(TweetsSession.shared.user.lastName)",
                "bodyUrl": "empty",
                "authorUrl": "empty"
                
                ] as [String : Any]
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization":  TweetsSession.shared.idToken!
            ]
            if let s3URLString = s3URL?.absoluteString {
                parameters["bodyUrl"] = s3URLString
            }
            if let authorUrl = TweetsSession.shared.user?.photoUrl {
                parameters["authorUrl"] = authorUrl
            }
            return networkProxy.request(.createPost, parameters: parameters, headers: headers)
            .map { data in
                let decoder = JSONDecoder()
                return try decoder.decode(Status.self, from: data)
            }
        }
    }
    
    func getFeed(for handle: String, sortKey: String?) -> Promise<StatusListResponse> {
        return networkProxy.codableRequest(.getFeed(handle, sortKey), as: StatusListResponse.self)
    }
    
    func getStory(for handle: String, sortKey: String?) -> Promise<StatusListResponse> {
        return networkProxy.codableRequest(.getStory(handle, sortKey), as: StatusListResponse.self)
    }
    
    func getStatuses(for hashtag: String) -> Promise<StatusListResponse> {
        return networkProxy.codableRequest(.getHashtagStatus(hashtag), as: StatusListResponse.self)
    }
    
}

struct UserListResponse: Codable {
    var lastEvaluatedKey: LastEvaluatedKey?
    var items: [User]?
}

struct StatusListResponse: Codable {
    var lastEvaluatedKey: LastEvaluatedKey?
    var items: [Status]?
}

struct LastEvaluatedKey: Codable {
    var partitionKey: String?
    var sortKey: String?
}
