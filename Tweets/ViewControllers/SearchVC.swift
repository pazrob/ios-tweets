//
//  TableViewController.swift
//  LDS Worker
//
//  Created by Campus Life Design 1 on 6/26/18.
//  Copyright © 2018 Robinson Paz. All rights reserved.
//

import UIKit
import MessageUI

class SearchVC: UIViewController {
    
    let searchController = UISearchController(searchResultsController: nil)
    let userNetwork = UserNetworkController()
    let statusNetwork = StatusNetworkController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Color.pageBackground.value
        setUpNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        searchController.searchBar.becomeFirstResponder()
    }
    
    func setUpNavigationBar() {
        navigationItem.title = "Search @ or #"
        navigationController?.navigationBar.tintColor = Color.primary.value
        navigationController?.navigationBar.prefersLargeTitles = true
        
        searchController.searchBar.delegate = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Try with @..."
        navigationItem.searchController = searchController
        definesPresentationContext = true
        navigationItem.hidesSearchBarWhenScrolling = true
    }
}

extension SearchVC: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let searchText = searchBar.text else { return }
        
        startLoading()
        if searchText.hasPrefix("@") {
            userNetwork.searchUser(handle: searchText)
                .done { user in
                    let userHomeVC = UserHomeVC(with: user)
                    userHomeVC.title = searchText
                    let userNav = UINavigationController(rootViewController: userHomeVC)
                    self.present(userNav, animated: true, completion: nil)
            }.catch { error in
                log.error(error)
                UIAlertController.presentError(viewController: self, message: "User not found")
            }.finally {
                self.stopAnimating()
            }
            
        } else if searchText.hasPrefix("#") {
            statusNetwork.getStatuses(for: searchText)
                .done { statusListResponse in
                    if statusListResponse.items!.isEmpty {
                        UIAlertController.presentError(viewController: self, message: "No status matches the hashtag 😓")
                    } else {
                        let feedVC = FeedVC()
                        feedVC.title = searchText
                        feedVC.updateData(statusList: statusListResponse.items!, lastEvaluatedKey: statusListResponse.lastEvaluatedKey?.sortKey)
                        let userNav = UINavigationController(rootViewController: feedVC)
                        self.present(userNav, animated: true, completion: nil)
                    }
                }.catch { error in
                log.error(error)
                UIAlertController.presentError(viewController: self, message: "No status matches the hashtag 😓")
            }.finally {
                self.stopAnimating()
            }
            
        } else {
            stopAnimating()
            UIAlertController.presentError(viewController: self,
                                           title: "Search Error",
                                           message: "Please using the prefix @ or #")
        }
    }
}
