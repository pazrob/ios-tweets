//
//  LoginBaseVC.swift
//  Bacon
//
//  Created by Robinson Paz Jesus on 9/6/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import UIKit

class LoginBaseVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func handleSignIn(_ sender: UIButton) {
        let router = AuthRouter()
        router.presentAuthRouter(viewController: self, for: .signIn)
    }
    
    @IBAction func handleSignUp(_ sender: UIButton) {
        let router = AuthRouter()
        router.presentAuthRouter(viewController: self, for: .signUp)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

}
