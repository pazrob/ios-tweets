//
//  LoginVC.swift
//  Bacon
//
//  Created by Robinson Paz Jesus on 9/5/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var handleField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var signInButton: UIButton!
    
    let authRouter = AuthRouter()
    let userNetworkController = UserNetworkController()
    
    // MARK: - Observer functions
    
    @IBAction func textEditingChanged(_ sender: UITextField) {
        //Check if handle and password
        let isValidHandler = String.isValidHanle(handle: handleField.text)
        let isPasswordValid = String.isValidPassword(password: passwordField.text)
        //Change button's properties based on email and password
        signInButton.setEnable(with: isValidHandler && isPasswordValid)
    }
    
    // MARK: - UI handlers
    
    @IBAction func handleLogin(_ sender: Any?) {
        guard let handle = handleField.text else { return }
        guard let password = passwordField.text else { return }
        
        //Hide keyboard
        view.endEditing(false)
        startLoading()
        
        userNetworkController.login(handle: handle, password: password, completed: { success, errorMessage in
            self.stopAnimating()
            if success {
                AuthRouter.rootToHome(with: handle)
            } else {
                UIAlertController.presentError(viewController: self, message: errorMessage)
            }  
        })
    }
    
    @IBAction func handleSignUp(_ sender: Any) {
        //Get parent router and switch view controller
        guard let authRouter = parent as? AuthRouter else { return }
        authRouter.switchToAuthType(.signUp)
    }
}

//This section manages small interactions
extension LoginVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        signInButton.layer.masksToBounds = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        handleField.becomeFirstResponder()
//        handleField.text = "@paz27"
//        passwordField.text = "asdasd"
//        handleLogin(nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        view.endEditing(true)
    }
}
