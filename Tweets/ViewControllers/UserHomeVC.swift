//
//  UserHomeVC.swift
//  Tweets
//
//  Created by Robinson Paz Jesus on 9/6/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import AlamofireImage

enum CellTypes {
    case story
    case following
    case followers
}

class UserHomeVC: UIViewController {
    
    var atCellType: CellTypes = .story
    private var user: User?
    var handle: String?
    let headerView = HeaderView()
    
    var story: [Status] = []
    var following: [User] = []
    var followers: [User] = []
    
    lazy var dataSource = DataSource(viewController: self, delegate: self)
    let followerController = FollowersNetworkController()
    let systemUpdater = SystemUpdater()
    
    var storyLastKey: String?
    var followingLastKey: String?
    var followerLastKey: String?
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableView.automaticDimension
        tableView.contentInset = UIEdgeInsets(top: 32, left: 0, bottom: 32, right: 0)
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        view.addSubview(tableView)
        tableView.fillSuperview()
        return tableView
    }()
    
    var menuBarData = ["STORY", "FOLLOWING", "FOLLOWERS"]
    let refreshControl = UIRefreshControl()
    
    init(with user: User) {
        self.user = user
        super.init(nibName: nil, bundle: nil)
    }
    
    init(with handle: String) { //TODO: When am I using this guy
        self.handle = handle
        super.init(nibName: nil, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource.user = user
        systemUpdater.delegateContent = self
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        
        tableView.refreshControl = refreshControl
        tableView.refreshControl?.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        
        headerView.headerViewDelegate = self
        navigationItem.largeTitleDisplayMode = .never
        view.backgroundColor = Color.pageBackground.value
        dataSource.setUpTableView(tableView)
        setupHeaderView()
        refreshData()
    }
    
    @objc private func refreshData() {
        
        guard let handle = user?.handle else { return }
        systemUpdater.fetchContent(handle: handle) { [unowned self] in
            self.refreshControl.endRefreshing()
            self.headerView.reloadData()
            self.refreshDataSource(dataType: self.atCellType)
        }
    }
    
    func setupHeaderView() {
        headerView.profileImage.image = UIImage(named: "default_user")
        if let urlString = user?.photoUrl, let url = URL(string: urlString) {
             headerView.profileImage.af_setImage(withURL: url)
        }
        
        // User photo
        self.headerView.followButton.isHidden = true
        if user?.handle == TweetsSession.shared.handle {
            headerView.updateSizeMissingButton()
        } else if let handle = TweetsSession.shared.user.handle, let handleFollowing = user?.handle {
            followerController.isFollowingRelation(handleUser: handle, handleFollowing: handleFollowing)
                .done { amI in
                    self.headerView.followButton.setStyle(on: !amI)
                    self.headerView.followButton.isHidden = false
            }.catch { error in
                log.error(error)
            }
        }
        
        if let firstName = user?.firstName, let lastName = user?.lastName {
            headerView.nameLabel.text = "\(firstName) \(lastName)"
        }
        headerView.handleLabel.text = user?.handle
        headerView.menuBar.delegate = self
        
        let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
        var headerFrame = headerView.frame
        headerFrame.size.height = height
        headerView.frame = headerFrame
        tableView.tableHeaderView = headerView
    }
    
    func refreshDataSource(dataType: CellTypes) {
        dataSource.rows.removeAll()
        if dataType == .story {
            story.forEach({ dataSource.rows.append(.feed($0 ))})
        } else if dataType == .following {
            following.forEach({ dataSource.rows.append(.following($0))})
        } else {
            followers.forEach({ dataSource.rows.append(.follower($0))})
        }
        dataSource.reloadTable()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension UserHomeVC: HeaderViewDelegate {
    
    func followButtonDidSelect(button: UIButton, isToFollow: Bool) {
        startLoading()
        if !isToFollow { //follow
            followerController.startFollowing(user: user!)
                .done { _ in
                    NotificationCenter.default.post(name: .newUserBeingFollowed, object: self.user)
            }.catch { error in
                log.error(error)
            }.finally {
                self.stopAnimating()
            }
        } else if let handle = user?.handle { //unfollow
            followerController.stopFollowing(handle: handle)
                .done { _ in
                    // reload following
            }.catch { error in
                log.error(error)
            }.finally {
                self.stopAnimating()
            }
        }
    }
}

extension UserHomeVC: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        handleLink(viewController: self, URL: URL)
        return false
    }
}

extension UserHomeVC: MenuBarDelegate {
    
    // MARK: - MenuBarDelegate functions
    func numberOfTabs(in menuBar: MenuBar) -> Int {
        return menuBarData.count
    }
    
    func menuBar(menuBar: MenuBar, didSelectButtonAt index: Int, previousSelection: Int) {
        guard index != previousSelection else { return }
        if index == 0 {
            refreshDataSource(dataType: .story)
            atCellType = .story
        } else if index == 1 {
            refreshDataSource(dataType: .following)
            atCellType = .following
        } else {
            refreshDataSource(dataType: .followers)
            atCellType = .followers
        }
    }
    
    func menuBar(menuBar: MenuBar, textForButtonAt index: Int) -> (Int, String) {
        var indexCount = 0
        if index == 0 {
            indexCount = story.count
        } else if index == 1 {
            indexCount = following.count
        } else {
            indexCount = followers.count
        }
        return (indexCount, menuBarData[index])
    }
}

extension UserHomeVC: DataSourceDelegate {
    
    func loadAnotherPage() {
        
        switch self.headerView.menuBar.previousSelection {
        case 0: // Load story
            systemUpdater.fetchMoreStory(handle: user!.handle!, sortKey: storyLastKey, completed: {})
        case 1: // Load following
            systemUpdater.fetchMoreFollowing(handle: user!.handle!, sortKey: followingLastKey, completed: {})
        case 2: // Load followers
            systemUpdater.fetchMoreFollowers(handle: user!.handle!, sortKey: followerLastKey, completed: {})
        default:
            print("Should not be here")
        }
    }
}

extension UserHomeVC: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named: "empty-default")!
    }
    
    func verticalOffset(forEmptyDataSet scrollView: UIScrollView!) -> CGFloat {
        return 200
    }
}

extension UserHomeVC: SystemUpdaterDelegateContent {
    
    func systemUpdater(forFollowees: [User], isNewPage: Bool, lastKey: String?) {
        self.followingLastKey = lastKey
        if isNewPage {
            self.following.append(contentsOf: forFollowees)
        } else {
            self.following = forFollowees
        }
        refreshDataSource(dataType: atCellType)
        self.headerView.reloadData() //TODO: Loads three times?
    }
    
    func systemUpdater(forFollowers: [User], isNewPage: Bool, lastKey: String?) {
        self.followerLastKey = lastKey
        if isNewPage {
            self.followers.append(contentsOf: forFollowers)
        } else {
            self.followers = forFollowers
        }
        refreshDataSource(dataType: atCellType)
        self.headerView.reloadData()
    }
    
    func systemUpdater(forStory: [Status], isNewPage: Bool, lastKey: String?) {
        self.storyLastKey = lastKey
        if isNewPage {
            self.story.append(contentsOf: forStory)
        } else {
            self.story = forStory
        }
        refreshDataSource(dataType: atCellType)
        self.headerView.reloadData()
    }
    
    func systemUpdater(forFeed: [Status], isNewPage: Bool, lastKey: String?) {}
}


extension Notification.Name {
    static let newUserBeingFollowed = Notification.Name("newUserBeingFollowed")
}
