//
//  ShiftTabStripVC.swift
//  Bacon
//
//  Created by Robinson Paz Jesus on 3/6/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class FavoriteParentVC: ButtonBarPagerTabStripViewController {
    
    var child_1 = FavoriteVC(type: .following)
    var child_2 = FavoriteVC(type: .followers)
    
    override func viewDidLoad() {
        setupTopBar()   //keep below super.viewDidload()
        super.viewDidLoad()
        _ = child_1.view
        _ = child_2.view
    }
    
    // MARK: - PagerTabStripDataSource
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        return [child_1, child_2]
    }
    
    private func setupTopBar() {
        //change selected bar color
        settings.style.buttonBarBackgroundColor = .white
        settings.style.buttonBarItemBackgroundColor = .white
        settings.style.selectedBarBackgroundColor = Color.darkText.value
        settings.style.buttonBarItemFont = UIFont.boldSystemFont(ofSize: 14)
        settings.style.selectedBarHeight = 2.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = Color.primary.value // ???
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = Color.lightText.value
            newCell?.label.textColor = Color.darkText.value
        }
    }
}
