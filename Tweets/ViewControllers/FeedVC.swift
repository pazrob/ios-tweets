//
//  FeedViewController.swift
//  Tweets
//
//  Created by Robinson Paz Jesus on 9/4/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import UserNotifications

import NVActivityIndicatorView

protocol FeedVCDelegate {
    func returnStatus(newStatus: Status)
}

class FeedVC: UIViewController {
    
    var user: User? {
        didSet {
            setupHeaderView()
            dataSource.user = user
        }
    }
    
    // Views
    let headerView = HeaderView()
    private let refreshControl = UIRefreshControl()
    lazy var feedTableView: UITableView = {
        let tableView = UITableView()
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableView.automaticDimension
        tableView.contentInset = UIEdgeInsets(top: 32, left: 0, bottom: 32, right: 0)
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.delaysContentTouches = true
        return tableView
    }()
    
    // Entities
    var controllerType = FeedType.feed
    let photoManager = MediaManager()
    lazy private var dataSource = DataSource(viewController: self, delegate: self)
    var lastEvaluatedKey: String?
    let systemUpdater = SystemUpdater()
    
    convenience init(type: FeedType) {
        self.init(nibName: nil, bundle: nil)
        self.controllerType = type
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        feedTableView.emptyDataSetSource = self
        feedTableView.emptyDataSetDelegate = self
        systemUpdater.delegateContent = self
        photoManager.delegate = self
        setupTableView()
        navigationItem.largeTitleDisplayMode = .never
    }
    
    private func setupTableView() {
        view.addSubview(feedTableView)
        feedTableView.fillSuperview()
        dataSource.setUpTableView(feedTableView)
        feedTableView.refreshControl = refreshControl
        feedTableView.refreshControl?.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
    }
    
    @objc private func refreshData(_ sender: Any) {
        guard let handle = user?.handle else { return }
        reloadData(handle: handle, sortKey: nil)
    }
    
    func loadOneStatusOnly(with status: Status) {
        title = "Detailed View"
        hideShowMoreButton()
        dataSource.rows.removeAll()
        dataSource.rows.append(.feed(status))
        dataSource.reloadTable()
    }
    
    func hideShowMoreButton() {
        dataSource.showLoadMoreButton = false
    }
    
    private func setupHeaderView() {
        guard let user = self.user else { return }
        if let photoString = user.photoUrl, let photoURL = URL(string: photoString) {
            headerView.profileImage.af_setImage(withURL: photoURL) { [unowned self] _ in
                TweetsSession.shared.photo = self.headerView.profileImage.image
            }
        }
        headerView.nameLabel.text = "\(user.firstName) \(user.lastName)"
        headerView.handleLabel.text = user.handle
        headerView.menuBar.isHidden = true
        headerView.followButton.isHidden = true
        headerView.updateSizeForProfile()
        headerView.cameraIcon.isHidden = false
        dataSource.setUpTableHeaderView(headerView: headerView)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleUpdateImage(_:)))
        headerView.profileImage.addGestureRecognizer(tap)
        headerView.profileImage.isUserInteractionEnabled = true
    }
    
    func updateData(statusList: [Status], lastEvaluatedKey: String?) {
        if let key = lastEvaluatedKey {
            self.lastEvaluatedKey = key
        }
        statusList.forEach({ dataSource.rows.append(.feed($0)) })
        dataSource.rows.sort(by: { $0.getStatus()!.timestamp! >  $1.getStatus()!.timestamp! })
        feedTableView.reloadData()
    }
    
    @objc func handleUpdateImage(_ sender: UITapGestureRecognizer) {
        photoManager.presentPhotoPicker(self)
    }
    
    enum FeedType {
        case feed
        case story
    }
}

extension FeedVC: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        let image = UIImage(named: controllerType == .feed ? "empty-feed" : "empty-story")!
        return image
    }
}

extension FeedVC: SystemUpdaterDelegateContent {
    
    func systemUpdater(forFeed: [Status], isNewPage: Bool, lastKey: String?) {
        lastEvaluatedKey = lastKey
        if isNewPage {
            dataSource.appendStatusPage(page: forFeed)
        } else {
            dataSource.updateDataStory(with: forFeed)
        }
        dataSource.updateButtonNormal()
        refreshControl.endRefreshing()
    }
    
    func systemUpdater(forStory: [Status], isNewPage: Bool, lastKey: String?) {
        lastEvaluatedKey = lastKey
        if isNewPage {
            dataSource.appendStatusPage(page: forStory)
        } else {
            dataSource.updateDataStory(with: forStory)
        }
        dataSource.updateButtonNormal()
        refreshControl.endRefreshing()
    }
    
    func systemUpdater(forFollowees: [User], isNewPage: Bool, lastKey: String?) {}
    
    func systemUpdater(forFollowers: [User], isNewPage: Bool, lastKey: String?) {}
}

extension FeedVC: MediaManagerDelegate {
    func mediaManager(retreivedUrl: URL) {
        print("Should not be here")
    }
    
    func mediaManager(retreivedUrl: URL, retreivedImage: UIImage) {
        startLoading()
        let imageController = ImageNetworkController()
        imageController.updateUserPhoto(originalUrl: retreivedUrl, image: retreivedImage) { image in
            if let image = image {
                self.headerView.profileImage.image = image
                self.feedTableView.reloadData()
            }
            self.stopAnimating()
        }
    }
}

extension FeedVC: DataSourceDelegate {
    
    func reloadData(handle: String, sortKey: String?) {
        if controllerType == .feed {
            systemUpdater.fetchMoreFeed(handle: handle, sortKey: sortKey, completed: {})
        } else {
            systemUpdater.fetchMoreStory(handle: handle, sortKey: sortKey, completed: {})
        }
    }
    
    func loadAnotherPage() {
        guard let handle = TweetsSession.shared.handle else { return }
        guard let key = self.lastEvaluatedKey, !key.isEmpty else {
            dataSource.updateButtonEnd()
            return
        }
        reloadData(handle: handle, sortKey: lastEvaluatedKey)
    }
}


