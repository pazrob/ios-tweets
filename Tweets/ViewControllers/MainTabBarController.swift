//
//  MainTabBarController.swift
//  Tweets
//
//  Created by Robinson Paz Jesus on 9/4/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController {
    
    let systemUpdater = SystemUpdater()
    var handle: String?
    
    // Children view controllers
    let favoriteStoryboard = UIStoryboard(name: "Favorite", bundle: nil)
    let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
    let feedVC = FeedVC(type: .feed)
    let searchVC = SearchVC()
    lazy var favoritesVC = favoriteStoryboard.instantiateViewController(withIdentifier: "favoriteParentVC") as! FavoriteParentVC
    let storyVC = FeedVC(type: .story)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        setupViewControllers()
        fetchUser()
    }
    
    private func fetchUser() {
        systemUpdater.delegateUser = self
        systemUpdater.fetchUser(handle: self.handle!, completed: {})
    }
    
    func setupViewControllers() {
        _ = feedVC.view
        _ = storyVC.view
        _ = favoritesVC.view
        
        // FeedVC
        feedVC.navigationItem.title = "Feed"
        feedVC.tabBarItem = UITabBarItem(title: nil, image: #imageLiteral(resourceName: "house.png"), selectedImage: nil)
        let feedNav = UINavigationController(rootViewController: feedVC)
        feedNav.view.backgroundColor = Color.pageBackground.value
        
        // SearchVC
        searchVC.tabBarItem = UITabBarItem(title: nil, image: #imageLiteral(resourceName: "glass.png"), selectedImage: nil)
        let searchNav = UINavigationController(rootViewController: searchVC)
        searchNav.view.backgroundColor = Color.pageBackground.value
        
        // Placeholder
        let placeholderVC = UIViewController()
        placeholderVC.tabBarItem = UITabBarItem(title: nil, image: #imageLiteral(resourceName: "plus.png"), selectedImage: nil)
        
        // Favorite
        favoritesVC.navigationItem.title = "Favorites"
        favoritesVC.tabBarItem = UITabBarItem(title: nil, image: #imageLiteral(resourceName: "heart.png"), selectedImage: nil)
        let favoritesNav = UINavigationController(rootViewController: favoritesVC)
        favoritesNav.view.backgroundColor = Color.pageBackground.value
        
        // StoryVC
        storyVC.navigationItem.title = "Story"
        storyVC.tabBarItem = UITabBarItem(title: nil, image: #imageLiteral(resourceName: "person.png"), selectedImage: nil)
        storyVC.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Log out", style: .plain, target: self, action: #selector(handleLogout))
        let storyNav = UINavigationController(rootViewController: storyVC)
        storyNav.view.backgroundColor = Color.pageBackground.value
        
        // Add them
        viewControllers = [feedNav, searchNav, placeholderVC, favoritesNav, storyNav]
        for tabBarItem in tabBar.items! {
            tabBarItem.title = ""
            tabBarItem.imageInsets = UIEdgeInsets(top: 8, left: 0, bottom: -16, right: 0)
        }
        tabBar.tintColor = Color.primary.value
    }
    
    @objc func handleLogout() {
        let logoutAction = UIAlertAction(title: "Log Out", style: .default) { (_) in
            let user = pool.currentUser()
            user?.signOut()
            pool.clearLastKnownUser()
            AuthRouter.rootToAuth()
            // TODO: erase all singleton, data, etc. locally.
        }
        UIAlertController.presentSheet(viewController: self, actions: [logoutAction])
    }
}

extension MainTabBarController: UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        guard let viewControllers = viewControllers else { return true }
        return viewController != viewControllers[2]
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        guard let items = tabBar.items else { return }
        if item == items[2] {
            // TODO: Block till the user response has come.
            let createPostVC = mainStoryboard.instantiateViewController(withIdentifier: "CreatePostVC") as! CreatePostVC
            createPostVC.delegate = self
            let nc = UINavigationController(rootViewController: createPostVC)
            nc.navigationBar.isHidden = true
            presentAsStork(nc)
        }
    }
}

extension MainTabBarController: SystemUpdaterDelegateUser {
    
    func systemUpdater(forUser user: User) {
        guard let handle = self.handle else { return }
        TweetsSession.shared.updateUser(user: user)
        feedVC.reloadData(handle: handle, sortKey: nil)
        favoritesVC.child_1.reloadData(handle: handle, sortKey: nil)
        favoritesVC.child_2.reloadData(handle: handle, sortKey: nil)
        storyVC.user = user
        storyVC.reloadData(handle: handle, sortKey: nil)
    }
}

extension MainTabBarController: CreatePostVCDelegate {
    
    func returnStatus(newStatus: Status) {
        storyVC.updateData(statusList: [newStatus], lastEvaluatedKey: nil)
    }
}
