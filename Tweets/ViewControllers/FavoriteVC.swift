//
//  ShiftCollectionVC.swift
//  Bacon
//
//  Created by Robinson Paz Jesus on 3/6/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import DZNEmptyDataSet
import NVActivityIndicatorView

class FavoriteVC: UIViewController {
    
    lazy var dataSource = DataSource(viewController: self, delegate: self)
    let systemUpdater = SystemUpdater()
    let refreshControl = UIRefreshControl()
    var controllerType: ControllerType = .following
    var lastEvaluatedKey: String?
    
    lazy var usersTableView: UITableView = {
        let tableView = UITableView()
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableView.automaticDimension
        tableView.contentInset = UIEdgeInsets(top: 32, left: 0, bottom: 32, right: 0)
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        view.addSubview(tableView)
        tableView.fillSuperview()
        return tableView
    }()
    
    init(type: ControllerType) {
        self.controllerType = type
        super.init(nibName: nil, bundle: nil)
    }
    
    convenience init() {
        self.init(type: .following)
    }
    
    // I am trying to find a way to feed the newledy fetch users to my unborn PavoriteVC
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Color.pageBackground.value
        systemUpdater.delegateContent = self
        usersTableView.emptyDataSetSource = self
        usersTableView.emptyDataSetDelegate = self
        dataSource.setUpTableView(usersTableView)
        setupNotifications()
        usersTableView.refreshControl = refreshControl
        usersTableView.refreshControl?.addTarget(self, action: #selector(refreshData), for: .valueChanged)
    }
    
    @objc func refreshData() {
        guard let handle = TweetsSession.shared.handle else { return }
        reloadData(handle: handle, sortKey: nil)
    }
    
    private func setupNotifications() {
        let isFollowing = controllerType == .following
        if isFollowing {
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(addNewUser(_:)),
                                                   name: .newUserBeingFollowed,
                                                   object: nil)
        }
    }
    
    @objc private func addNewUser(_ notification: Notification) {
        if let newUser = notification.object as? User {
            dataSource.appendRow(row: controllerType == .following ? .following(newUser) : .follower(newUser) )
        }
    }
    
    enum ControllerType {
        case following
        case followers
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension FavoriteVC: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        let image = UIImage(named: controllerType == .following ? "empty-following" : "empty-followers")!
        return image
    }
}

extension FavoriteVC: DataSourceDelegate {
    
    func reloadData(handle: String, sortKey: String?) {
        if controllerType == .following {
            systemUpdater.fetchMoreFollowing(handle: handle, sortKey: sortKey, completed: {})
        } else {
            systemUpdater.fetchMoreFollowers(handle: handle, sortKey: sortKey, completed: {})
        }
    }
    
    func loadAnotherPage() {
        guard let handle = TweetsSession.shared.user.handle else { return }
        guard let key = self.lastEvaluatedKey, !key.isEmpty else {
            dataSource.updateButtonEnd()
            return
        }
        reloadData(handle: handle, sortKey: lastEvaluatedKey)
    }
}

extension FavoriteVC: SystemUpdaterDelegateContent {
    func systemUpdater(forFollowees: [User], isNewPage: Bool, lastKey: String?) {
        lastEvaluatedKey = lastKey
        if isNewPage {
            dataSource.appendFollowingPage(page: forFollowees)
        } else {
            dataSource.updateDataFollowing(with: forFollowees)
        }
        dataSource.updateButtonNormal()
        refreshControl.endRefreshing()
    }
    
    func systemUpdater(forFollowers: [User], isNewPage: Bool, lastKey: String?) {
        lastEvaluatedKey = lastKey
        if isNewPage {
            dataSource.appendFollowersPage(page: forFollowers)
        } else {
            dataSource.updateDataFollowers(with: forFollowers)
        }
        dataSource.updateButtonNormal()
        refreshControl.endRefreshing()
    }
    
    func systemUpdater(forStory: [Status], isNewPage: Bool, lastKey: String?) { }
    
    func systemUpdater(forFeed: [Status], isNewPage: Bool, lastKey: String?) { }
    
}

// MARK: - Page titles

extension FavoriteVC: IndicatorInfoProvider {
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        let displayText = controllerType == .following ? "FOLLOWING" : "FOLLOWERS"
        return IndicatorInfo(title: displayText)
    }
}

