//
//  RegisterVC.swift
//  Bacon
//
//  Created by Robinson Paz Jesus on 9/6/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import UIKit

class RegisterVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var photoButton: UIButton!
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var handleField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var signUpButton: UIButton!
    
    // Variables
    let authRouter = AuthRouter()
    let mediaManager = MediaManager()
    let userNetwork = UserNetworkController()
    
    var selectedUrl: URL?
    var selectedImage: UIImage?
    
    // MARK: - UI handlers
    
    @IBAction func handleSignUp(_ sender: UIButton) {
        // Get values
        guard let firstName = firstNameField.text else { return }
        guard let lastName = lastNameField.text else { return }
        guard let handle = handleField.text else { return }
        guard let password = passwordField.text else { return }
        
        startLoading()
        userNetwork.register(firstName: firstName,
                                lastName: lastName,
                                handle: handle,
                                password: password) { success, errorMessage in
                                    self.stopAnimating()
                                    if success {
                                        AuthRouter.rootToHome(isNewUser: true, with: handle)
                                    } else {
                                        UIAlertController.presentError(viewController: self, message: errorMessage)
                                    }
        }
    }
    
    @IBAction func handleSignIn(_ sender: UIButton) {
        //Get parent router and switch view controller
        guard let authRouter = parent as? AuthRouter else { return }
        authRouter.switchToAuthType(.signIn)
    }
    
    @IBAction func handleUpdatePhoto(_ sender: Any) {
        mediaManager.presentPhotoPicker(self)
    }
    
    @IBAction func textFieldEditingDidChange(_ sender: Any) {
        let isFormFilled = String.isValidNaming(check: [firstNameField.text, lastNameField.text])
        let isHandleCorrect = String.isValidHanle(handle: handleField.text)
        let isPasswordFilled = String.isValidPassword(password: passwordField.text)
        signUpButton.setEnable(with: isFormFilled && isHandleCorrect && isPasswordFilled)
    }
}

extension RegisterVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mediaManager.delegate = self
        photoButton.layer.borderColor = Color.primary.value.cgColor
        photoButton.layer.borderWidth = 1
        photoButton.addShadow()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        firstNameField.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        view.endEditing(true)
    }
}

extension RegisterVC: MediaManagerDelegate {
    
    func mediaManager(retreivedUrl: URL, retreivedImage: UIImage) {
        self.photoButton.setImage(selectedImage, for: .normal)
        self.photoButton.clipsToBounds = true
        self.photoButton.layer.masksToBounds = true
        self.selectedUrl = retreivedUrl
        self.selectedImage = retreivedImage
    }
    
    func mediaManager(retreivedUrl: URL) { }
}
