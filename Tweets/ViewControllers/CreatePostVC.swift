//
//  CreatePostVC.swift
//  Tweets
//
//  Created by Robinson Paz Jesus on 9/6/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SPFakeBar
import AVKit
import MobileCoreServices

protocol CreatePostVCDelegate: class {
    func returnStatus(newStatus: Status)
}

class CreatePostVC: UIViewController {
    
    let imageController = ImageNetworkController()
    let statusNetworkController = StatusNetworkController()
    weak var delegate: CreatePostVCDelegate?
    let mediaManager = MediaManager()
    var imageUrl: URL?
    var videoUrl: URL?
    
    let navBar = SPFakeBarView(style: .stork)
    @IBOutlet weak var photoImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var handleLabel: UILabel!
    @IBOutlet weak var contentTextView: IQTextView!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var videoButton: UIButton!
    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var photoHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var videoHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mediaManager.delegate = self
        contentTextView.delegate = self
        setupViews()
    }
    
    func setupViews() {
        view.backgroundColor = Color.pageBackground.value
        self.navBar.titleLabel.text = "CREATE POST"
        self.navBar.leftButton.setTitle("Cancel", for: .normal)
        self.navBar.leftButton.addTarget(self, action: #selector(self.handleDismiss), for: .touchUpInside)
        
        self.navBar.rightButton.setTitle("Post", for: .normal)
        self.navBar.rightButton.addTarget(self, action: #selector(handlePost), for: .touchUpInside)
        self.navBar.rightButton.isEnabled = false
        self.navBar.rightButton.alpha = 0.5
        self.view.addSubview(self.navBar)
        
        self.photoImage.image = TweetsSession.shared.photo
        self.nameLabel.text = "\(TweetsSession.shared.user.firstName) \(TweetsSession.shared.user.lastName)"
        self.handleLabel.text = TweetsSession.shared.handle
    }
    
    @objc func handlePost() {
        guard let text = contentTextView.text else { return }
        startLoading()
         var possibleS3URL: URL?
        let dispatchGroup = DispatchGroup()
        
        if let mediaUrl = self.imageUrl {
            dispatchGroup.enter()
            imageController.uploadImage(originalUrl: mediaUrl, image: nil, isUserProfile: false) { s3URL in
                possibleS3URL = s3URL
                dispatchGroup.leave()
            }
        } else if let videoUrl = self.videoUrl {
            dispatchGroup.enter()
            imageController.uploadVideo(fileUrl: videoUrl) { s3URL in
                possibleS3URL = s3URL
                dispatchGroup.leave()
            }
        }
        
        dispatchGroup.notify(queue: .main) {
            self.statusNetworkController.postStatus(text: text, s3URL: possibleS3URL)
                .done { statusResponse in
                    
            }.catch { error in
                log.error(error)
                Toast.show(message: "FAILED POST", controller: self)
            }.finally {
                
            }
        }
        
        let statusResponse = Status(id: UUID().uuidString,
                                    text: text, handle: "@paz27",
                                    timestamp: Int(Date().timeIntervalSince1970),
                                    fullName: "Roby Pazito",
                                    authorUrl: nil,
                                    bodyUrl: "empty",
                                    viewURL: "empty")
            
        self.delegate?.returnStatus(newStatus: statusResponse)
        self.dismiss(animated: true, completion: nil)
        self.stopAnimating()
    }
    
    @IBAction func handleAddPhoto(_ sender: Any) {
        mediaManager.presentPhotoPicker(self)
    }
    
    @IBAction func handleAddVideo(_ sender: Any) {
        mediaManager.presentVideoPicker(self)
    }
    
    @IBAction @objc func handleDismiss(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
    }
}

extension CreatePostVC: MediaManagerDelegate {
    
    func mediaManager(retreivedUrl: URL) {
        self.videoUrl = retreivedUrl
        self.cameraButton.isHidden = true
    }
    
    func mediaManager(retreivedUrl: URL, retreivedImage: UIImage) {
        self.imageUrl = retreivedUrl
        self.cameraButton.imageView?.contentMode = .scaleAspectFill
        self.cameraButton.setImage(retreivedImage, for: .normal)
        self.videoButton.isHidden = true
        self.view.layoutIfNeeded()
    }
}

extension CreatePostVC: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        let lenght = textView.text.count
        counterLabel.text = "\(lenght)/200"
        if lenght > 0 {
            navBar.rightButton.isEnabled = true
            navBar.rightButton.alpha = 1
        } else {
            navBar.rightButton.isEnabled = false
            navBar.rightButton.alpha = 0.5
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = textView.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let changedText = currentText.replacingCharacters(in: stringRange, with: text)
        return changedText.count <= 200
    }
}
