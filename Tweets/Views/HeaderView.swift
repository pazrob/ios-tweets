//
//  HeaderView.swift
//  Tweets
//
//  Created by Robinson Paz Jesus on 9/8/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import UIKit

protocol PhotoImageDelegate {
    func updatePhoto()
}

protocol HeaderViewDelegate {
    func followButtonDidSelect(button: UIButton, isToFollow: Bool)
}

class HeaderView: UIView {
    
    var delegate: PhotoImageDelegate?
    var headerViewDelegate: HeaderViewDelegate?
    let deviceWidth =  UIScreen.main.bounds.size.width
    
    lazy var profileImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "default_user")
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 60
        imageView.clipsToBounds = true
        imageView.addBorder()
        imageView.tintColor = .white
        return imageView
    }()
    
    let cameraIcon: UIImageView = {
        let insets = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
        let image = #imageLiteral(resourceName: "camera.png").imageWithInsets(insets: insets)
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 16
        imageView.clipsToBounds = true
        imageView.tintColor = .white
        imageView.backgroundColor = Color.primary.value
        imageView.isHidden = true
        return imageView
    }()
    
    lazy var followButton: UIButton = {
        let button = UIButton(title: "Follow")
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(handleFollowSelect(_:)), for: .touchUpInside)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        button.backgroundColor = Color.primary.value
        button.roundCorners(radius: 25)
        button.tintColor = .clear
        return button
    }()
    
    let nameLabel = UIView.getLabel(font: UIFont.boldSystemFont(ofSize: 20))
    let handleLabel = UIView.getLabel(font: UIFont.systemFont(ofSize: 15, weight: .medium), color: .lightGray)
    let line = UIView.getContainer(color: UIColor.lightGray.withAlphaComponent(0.3))
    let menuBar = MenuBar()
    
    lazy var verticalStack: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [profileImage, nameLabel, handleLabel, followButton, line, menuBar])
        stack.axis = .vertical
        stack.alignment = .center
        stack.spacing = 8
        stack.setCustomSpacing(0, after: nameLabel)
        stack.setCustomSpacing(16, after: handleLabel)
        stack.setCustomSpacing(16, after: followButton)
        profileImage.anchor(heightCons: 120)
        let widthProfile = profileImage.widthAnchor.constraint(equalToConstant: 120)
        widthProfile.priority = .defaultHigh
        widthProfile.isActive = true
        followButton.anchor(heightCons: 50)
        let widthButton = followButton.widthAnchor.constraint(equalToConstant: 300)
        widthButton.priority = .defaultHigh
        widthButton.isActive = true
        line.anchor(heightCons: 1)
        let widthLine = line.widthAnchor.constraint(equalToConstant: deviceWidth - 40)
        widthLine.priority = .defaultHigh
        widthLine.isActive = true
        stack.addSubview(cameraIcon)
        cameraIcon.anchor(widthCons: 32, heightCons: 32)
        cameraIcon.centerXAnchor.constraint(equalTo: profileImage.centerXAnchor, constant: 40).isActive = true
        cameraIcon.centerYAnchor.constraint(equalTo: profileImage.centerYAnchor, constant: 40).isActive = true
        return stack
    }()
    
    lazy var size: CGSize = CGSize(width: self.frame.width, height: 340)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(verticalStack)
        verticalStack.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor)
        verticalStack.alignment = .center
        let widthMenu = menuBar.widthAnchor.constraint(equalToConstant: deviceWidth)
        widthMenu.priority = .defaultHigh
        widthMenu.isActive = true
    }
    
    func updateSizeForProfile() {
        size = CGSize(width: size.width, height: 210)
        invalidateIntrinsicContentSize()
    }
    
    func updateSizeMissingButton() {
        size = CGSize(width: size.width, height: 280)
        invalidateIntrinsicContentSize()
    }
    
    func reloadData() {
        menuBar.reloadCounts()
    }
    
    @objc func handleFollowSelect(_ sender: Any) {
        guard let button = sender as? UIButton else { return }
        button.setStyle(on: !button.isSelected)
        headerViewDelegate?.followButtonDidSelect(button: followButton, isToFollow: button.isSelected)
    }
    
    override var intrinsicContentSize: CGSize {
        return size
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
