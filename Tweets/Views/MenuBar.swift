//
//  MenuBar.swift
//  Tweets
//
//  Created by Robinson Paz on 9/4/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import UIKit

protocol MenuBarDelegate: class {
    
    /// Tells the view how many tabs will it have
    ///
    /// - Parameter menuBar: self view
    /// - Returns: number of tabs
    func numberOfTabs(in menuBar: MenuBar) -> Int
    
    /// Tells the view when it's selected
    ///
    /// - Parameters:
    ///   - menuBar: self view
    ///   - index: index selected
    ///   - previousSelection: previous index selected
    func menuBar(menuBar: MenuBar, didSelectButtonAt index: Int, previousSelection: Int)
    
    /// Feeds the tab text
    ///
    /// - Parameters:
    ///   - menuBar: self view
    ///   - index: index where text will be inserted
    /// - Returns: string for the tab
    func menuBar(menuBar: MenuBar, textForButtonAt index: Int) -> (Int,String)
}

extension MenuBarDelegate {
    /// Create an optional function that returns a default value  of zero
    func numberOfButtons(in menuBar: MenuBar) -> Int {
        return 0
    }
}

class MenuBar: UIView {
    
    var delegate: MenuBarDelegate? {
        didSet { setupView()  }
    }
    
    /// Left constraint use to move and animate the line view
    private var leftConstraint: NSLayoutConstraint?
    /// The  previous  selection on the tabs
    var previousSelection = 0
    
    var countLabels = [UILabel]()
    
    /// Main stack
    private lazy var stackView: UIStackView = {
        let stack = UIStackView()
        addSubview(stack)
        stack.fillSuperview()
        stack.distribution = .fillEqually
        stack.alignment = .center
        return stack
    }()
    
    /// Line view that animates
    private lazy var underlineView: UIView = {
        let view = UIView()
        view.backgroundColor = Color.primary.value
        addSubview(view)
        view.anchor(bottom: bottomAnchor, heightCons: 2)
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        stackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
        guard let delegate = delegate else { return }
        let numberOfTabs = delegate.numberOfTabs(in: self)
        guard numberOfTabs != 0 else { return }
        
        for index in 0..<numberOfTabs {
            let count = delegate.menuBar(menuBar: self, textForButtonAt: index).0
            let countLabel = getCustomLabel(count: count, index: index, font: UIFont.boldSystemFont(ofSize: 22), color: .black)
            countLabels.append(countLabel)
            let title = delegate.menuBar(menuBar: self, textForButtonAt: index).1
            let label = getCustomLabel(text: title, index: index, font: UIFont.boldSystemFont(ofSize: 12), color: .lightGray)
            stackView.addArrangedSubview(VerticalStackView(arrangedSubviews: [countLabel, label]))
        }
        let widthConstraintId = "underlineViewWidthConstraint"
        if let constraint = underlineView.constraint(byIdentifier: widthConstraintId) {
            constraint.isActive = false
            underlineView.removeConstraint(constraint)
        }
        
        let widthConstraint = underlineView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: (1/CGFloat(numberOfTabs)))
        widthConstraint.identifier = widthConstraintId
        widthConstraint.isActive = true
        underlineView.updateConstraintsIfNeeded()
        
        leftConstraint = underlineView.leftAnchor.constraint(equalTo: stackView.leftAnchor)
        leftConstraint?.isActive = true
    }
    
    private func getCustomLabel(text: String = "", count: Int? = nil, index: Int, font: UIFont, color: UIColor) -> UILabel {
        let label = UILabel()
        label.font = font
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(buttonSelected(_:)))
        label.addGestureRecognizer(tapGesture)
        label.isUserInteractionEnabled = true
        label.textAlignment = .center
        label.textColor = color
        label.tag = index
        label.text = count == nil ? text : String(count!)
        return label
    }
    
    @objc func buttonSelected(_ sender: UITapGestureRecognizer) {
        guard let label = sender.view as? UILabel else { return }
        let index = label.tag
        delegate?.menuBar(menuBar: self, didSelectButtonAt: index, previousSelection: previousSelection)
        previousSelection = index
        self.leftConstraint?.constant = CGFloat(index) * underlineView.frame.width
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseInOut, animations: {
            self.layoutIfNeeded()
        })
    }
    
    func reloadCounts() {
        guard let numberOfTabs = delegate?.numberOfTabs(in: self) else { return}
        for index in 0..<numberOfTabs {
            let count = delegate?.menuBar(menuBar: self, textForButtonAt: index).0
            countLabels[index].text = String(count!)
        }
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: self.frame.width, height: 60)
    }
}
