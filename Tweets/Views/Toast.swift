//
//  Toast.swift
//  Tweets
//
//  Created by Robinson Paz Jesus on 9/5/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import UIKit

enum ToastType {
    case positive
    case negative
}

class Toast {
    
    static var toastColor:UIColor = #colorLiteral(red: 0.1330000013, green: 0.1330000013, blue: 0.1330000013, alpha: 1)
    
    static let toastContainer: UIView = {
        let toast = UIView()
        toast.backgroundColor = toastColor
        toast.layer.cornerRadius = Sizes.toastCornerRadius.value
        toast.clipsToBounds  =  true
        toast.addSubview(toastMessage)
        toast.translatesAutoresizingMaskIntoConstraints = false
        return toast
    }()
    
    static let toastMessage: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.clipsToBounds  =  true
        label.translatesAutoresizingMaskIntoConstraints = false
        label.anchorCenterSuperview()
        return label
    }()
    
    public static func show(message: String, controller: UIViewController, type: ToastType? = nil) {
        
        if let toastType = type, toastType == .negative {
            toastColor = #colorLiteral(red: 0.1330000013, green: 0.1330000013, blue: 0.1330000013, alpha: 1)
        }

        toastMessage.text = message
        toastContainer.addSubview(toastMessage)
        toastMessage.anchorCenterSuperview()
        
        controller.view.addSubview(toastContainer)
        toastContainer.anchor(widthCons: Sizes.toastWidth.value, heightCons: Sizes.toastHeight.value)
        toastContainer.anchorCenterXToSuperview()
        
        //Create the toast right below view controller
        let bottomConstraint = toastContainer.topAnchor.constraint(equalTo: controller.view.safeAreaLayoutGuide.bottomAnchor)
        bottomConstraint.isActive = true
        
        layoutStackView()
        controller.view.layoutSubviews()
        controller.view.layoutIfNeeded()
        
        animateToast(with: bottomConstraint, in: controller, remove: toastContainer)
    }
    
    private static func layoutStackView() {
        toastContainer.addSubview(toastMessage)
        toastMessage.fillSuperview()
    }

    private static func animateToast(with bottomConstraint: NSLayoutConstraint, in controller: UIViewController, remove toastContainer: UIView, hide: Bool = true ) {
        
        let hideDelay: TimeInterval = 2
        let distanceFromBottom: CGFloat = 60
        
        //Animate toast up
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
            bottomConstraint.constant = -distanceFromBottom
            controller.view.layoutIfNeeded()
            
        }) { _ in
            if hide {
                //Animate toast down
                UIView.animate(withDuration: 1.0, delay: hideDelay, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
                    bottomConstraint.constant = distanceFromBottom + 40 + Sizes.toastHeight.value
                    controller.view.layoutIfNeeded()
                    
                }) { _ in
                    //Remove toast from superview
                    toastContainer.removeFromSuperview()
                }
            }
        }
    }
}

enum ToasMessage: String {
    case networkError = "ERROR, PLEASE TRY AGAIN LATER"
}
