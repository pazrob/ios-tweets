//
//  UserTableViewCell.swift
//  Tweets
//
//  Created by Robinson Paz Jesus on 9/6/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import UIKit
import AlamofireImage

protocol UserTableViewCellDelegate {
    func didTapFollowButton(cell: UITableViewCell, button: UIButton)
}

class UserTableViewCell: UITableViewCell {
    
    static let identifier = "UserTableViewCell"
    var delegate: UserTableViewCellDelegate?
    
    let profileImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "default_user")
        imageView.contentMode = .scaleAspectFill
        imageView.roundCorners(radius: 30)
        imageView.anchor(widthCons: 60, heightCons: 60)
        return imageView
    }()
    
    let nameLabel = UIView.getLabel(font: UIFont.boldSystemFont(ofSize: 15))
    let handleLabel = UIView.getLabel(font: UIFont.systemFont(ofSize: 15, weight: .medium), color: .lightGray)
    lazy var textStack = VerticalStackView(arrangedSubviews: [nameLabel, handleLabel])
    
    lazy var followButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(handleTap(_:)), for: .touchUpInside)
        button.anchor(widthCons: 120, heightCons: 50)
        button.roundCorners(radius: 25)
        button.tintColor = .clear
        button.isHidden = true //TODO: Decide if to keep this button or totally erase
        return button
    }()
    
    // Main stack
    lazy var mainStack: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [profileImage, textStack, followButton])
        stack.axis = .horizontal
        stack.alignment = .center
        stack.spacing = 16
        return stack
    }()
    
    lazy var  mainView: UIView = {
        let view = UIView.getContainer(color: .white)
        contentView.addSubview(view)
        view.fillSuperview(padding: UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 16))
        view.backgroundColor = .white
        // corner radius && shadow
        view.layer.cornerRadius = 10
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 0)
        view.layer.shadowOpacity = 0.2
        view.layer.shadowRadius = 6
        return view
    }()
    
    
    lazy var textContent: UILabel = {
        let textView = UILabel()
        textView.numberOfLines = 0
        return textView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        selectionStyle = .none
        mainView.addSubview(mainStack)
        mainStack.fillSuperview(padding: UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8))
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
    }
    
    func configure(with user: User, userType: RowType) {
        nameLabel.text = "\(user.firstName) \(user.lastName)"
        handleLabel.text = user.handle
        if let urlString = user.photoUrl, let url = URL(string: urlString) {
            profileImage.af_setImage(withURL: url)
        } else {
            profileImage.image = UIImage(named: "default_user")
        }
        if case .following = userType {
            followButton.setStyle(on: false)
        } else {
            followButton.setStyle(on: true)
        }
    }
    
    @objc private func handleTap(_ sender: UIButton) {
        delegate?.didTapFollowButton(cell: self, button: sender)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

