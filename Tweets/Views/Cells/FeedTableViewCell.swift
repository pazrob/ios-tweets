//
//  FeedTableViewCell.swift
//  Tweets
//
//  Created by Robinson Paz Jesus on 9/7/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import UIKit
import AlamofireImage
import AVKit

protocol FeedTableViewCellDelegate {
    func tableViewCell(didSelect cell: UITableViewCell)
}

class FeedTableViewCell: UITableViewCell {
    static let identifier = "FeedTableViewCell"
    var delegate: FeedTableViewCellDelegate?
    let mediaManager = MediaManager()
    
    let nameLabel = UIView.getLabel(font: UIFont.boldSystemFont(ofSize: 15))
    let handleLabel = UIView.getLabel(font: UIFont.systemFont(ofSize: 15, weight: .medium), color: .lightGray)
    let timeLabel = UIView.getLabel(font: UIFont.systemFont(ofSize: 13, weight: .medium), color: .lightGray)
    
    let profileImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "default_user")
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 30
        imageView.clipsToBounds = true
        imageView.anchor(widthCons: 60, heightCons: 60)
        return imageView
    }()
    
    lazy var textStack: UIStackView = {
        let stack = VerticalStackView(arrangedSubviews: [nameLabel, handleLabel, timeLabel])
        return stack
    }()
    
    lazy var topStack: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [profileImage, textStack])
        stack.isLayoutMarginsRelativeArrangement = true
        stack.layoutMargins = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        stack.axis = .horizontal
        stack.spacing = 16
        return stack
    }()
    
    lazy var mainStack: UIStackView = {
        let textContentStack = UIStackView(arrangedSubviews: [textContent])
        textContentStack.isLayoutMarginsRelativeArrangement = true
        textContentStack.layoutMargins = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        let stack = VerticalStackView(arrangedSubviews: [topStack, textContentStack, bodyImage, videoImage])
        stack.spacing = 16
        return stack
    }()
    
    let bodyImage: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 10
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.heightAnchor.constraint(lessThanOrEqualToConstant: 400).isActive = true
        return imageView
    }()
    
    lazy var playButton: UIButton = {
        let button = UIButton(type: .system)
        let image = UIImage(named: "video.png")?.withRenderingMode(.alwaysTemplate)
        button.setImage(image, for: .normal)
        button.tintColor = UIColor.white.withAlphaComponent(0.5)
        button.addTarget(self, action: #selector(handleVideoTap), for: .touchUpInside)
        return button
    }()
    
    lazy var videoImage: UIImageView = {
       let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 10
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        imageView.isHidden = true
        imageView.addSubview(playButton)
        playButton.fillSuperview()
        return imageView
    }()
    
    lazy var  mainView: UIView = {
        let view = UIView.getContainer(color: .white)
        contentView.addSubview(view)
        view.fillSuperview(padding: UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 16))
        view.backgroundColor = .white
        // corner radius && shadow
        view.layer.cornerRadius = 10
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 0)
        view.layer.shadowOpacity = 0.2
        view.layer.shadowRadius = 6
        return view
    }()
    
    lazy var textContent: UITextView = {
        let textView = UITextView()
        textView.isScrollEnabled = false
        textView.font = UIFont.systemFont(ofSize: 30)
        textView.isEditable = false
        textView.dataDetectorTypes = .link
        return textView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        selectionStyle = .none
        mainView.addSubview(mainStack)
        mainStack.fillSuperview(padding: UIEdgeInsets(top: 16, left: 0, bottom: 0, right: 0))
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        nameLabel.text = nil
        handleLabel.text = nil
        timeLabel.text = nil
        textContent.text = nil
        profileImage.image = UIImage(named: "default_user")
        bodyImage.image = nil
        videoImage.isHidden = true
    }
    
    func configure(with status: Status, user: User? = nil) {
        nameLabel.text = status.fullName
        handleLabel.text = status.handle
        timeLabel.text = status.getDate()
        textContent.attributedText = prepareText(with: status.text)
        
        // user photo: the one on header
        if user?.handle == TweetsSession.shared.handle {
            profileImage.image = TweetsSession.shared.photo
        } else if let recentUrlString = user?.photoUrl, let url = URL(string: recentUrlString) {
            profileImage.af_setImage(withURL: url)
        } else if let urlString = status.authorUrl, let url = URL(string: urlString) {
            // user photo: the one on status
            profileImage.af_setImage(withURL: url)
        }
        //  video photo
        if let urlString = status.bodyUrl, urlString.contains("video"), let url = URL(string: urlString) {
            videoImage.isHidden = false
            mediaManager.getThumbnailImageFromVideoUrl(url: url) { videoThumbnail in
                self.videoImage.image = videoThumbnail
            }
        } else if let urlString = status.bodyUrl, urlString != "empty", let url = URL(string: urlString) {
            bodyImage.af_setImage(withURL: url)
        } else {
            bodyImage.image = nil
        }
    }
    
    @objc private func handleVideoTap(_ sender: UITapGestureRecognizer) {
        delegate?.tableViewCell(didSelect: self)
    }
    
    func prepareText(with text: String?) -> NSMutableAttributedString {
        guard let text = text else { return NSMutableAttributedString() }
        let nsText = NSString(string: text)
        let result = NSMutableAttributedString(string: text, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15)])
        let words = text.components(separatedBy: " ")

        for word in words {
            if word.hasPrefix("@") || word.hasPrefix("#") && word.count > 1 {
                let myCustomAttribute = [NSAttributedString.Key.link: word,
                                         NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 15)] as [NSAttributedString.Key : Any]
                let matchRange:NSRange = nsText.range(of: word as String)
                result.addAttributes(myCustomAttribute, range: matchRange)
            }
        }
        return result
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
