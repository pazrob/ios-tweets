//
//  PhotoManager.swift
//  Tweets
//
//  Created by Robinson Paz Jesus on 10/17/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import UIKit
import MobileCoreServices
import AVFoundation
import Photos
import AVKit


protocol MediaManagerDelegate {
    func mediaManager(retreivedUrl: URL, retreivedImage: UIImage)
    func mediaManager(retreivedUrl: URL)
}

class MediaManager: NSObject {
    
    var delegate: MediaManagerDelegate?
    
    lazy var photoPicker: UIImagePickerController = {
        let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        return imagePicker
    }()
    
    func presentPhotoPicker(_ viewController: UIViewController) {
        checkAuthorization { [unowned self] success in
            if success {
                DispatchQueue.main.async {
                    viewController.present(self.photoPicker, animated: true, completion: nil)
                }
            }
        }
    }
    
    func presentVideoPicker(_ viewController: UIViewController) {
        photoPicker.mediaTypes = [kUTTypeMovie as String]
        photoPicker.videoQuality = .typeLow
        checkAuthorization { [unowned self] success in
            if success {
                DispatchQueue.main.async {
                    viewController.present(self.photoPicker, animated: true, completion: nil)
                }
            }
        }
    }
    
    func checkAuthorization(callback: @escaping (Bool)->Void) {
        switch PHPhotoLibrary.authorizationStatus() {
        case .authorized:
            callback(true)
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({ status in
                callback(status == .authorized)
            })
        case .denied:
            callback(false)
        default:
            callback(false)
        }
    }
    
    func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?)->Void)) {
        DispatchQueue.global().async { //1
            let asset = AVAsset(url: url) //2
            let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) //3
            avAssetImageGenerator.appliesPreferredTrackTransform = true //4
            let thumnailTime = CMTimeMake(value: 2, timescale: 1) //5
            do {
                let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
                let thumbImage = UIImage(cgImage: cgThumbImage) //7
                DispatchQueue.main.async { //8
                    completion(thumbImage) //9
                }
            } catch {
                print(error.localizedDescription) //10
                DispatchQueue.main.async {
                    completion(nil) //11
                }
            }
        }
    }
}

extension MediaManager: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    // Gets the image
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        var selectedImageFromPicker: UIImage?
        if let editedImage = info[.editedImage] as? UIImage { //Get edited image
            selectedImageFromPicker = editedImage
        } else if let originalImage = info[.originalImage] as? UIImage { //Get default image
            selectedImageFromPicker = originalImage
        }
        
        // Send chosen photo
        if let selectedImage = selectedImageFromPicker, let selectedUrl = info[.imageURL] as? URL {
            delegate?.mediaManager(retreivedUrl: selectedUrl, retreivedImage: selectedImage)
        }
        
        // Send chosen video
        if let videoURL = info[.mediaURL] as? URL {
            delegate?.mediaManager(retreivedUrl: videoURL)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    // Dismiss image picker view
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
