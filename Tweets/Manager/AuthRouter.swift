//
//  RouterVC.swift
//  Bacon
//
//  Created by Robinson Paz Jesus on 9/5/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import UIKit

class AuthRouter: UIViewController {
    
    //Get auth storyboard
    let authStoryboard = UIStoryboard(name: "Auth", bundle: nil)
    
    //Network requests
//    var userRequest: UserRequest? {
//        didSet {
//            print("Got for user request: ", userRequest)
//        }
//    }
//    var companyRequest: CompanyRequest? {
//        didSet {
//            print("Got for company request: ", companyRequest)
//        }
//    }
    
    
    //Present router in a navigation controller
    func presentAuthRouter(viewController: UIViewController, for type: AuthType) {
        
        let destination: UIViewController
        
        //The first transition
        if type == .signIn {
            title = "SIGN IN"
            destination = authStoryboard.instantiateViewController(withIdentifier: "signIn")
        } else {
            title = "SIGN UP"
            destination = authStoryboard.instantiateViewController(withIdentifier: "createAccount")
        }
        
        transition(to: destination)
        
        let navigationController = UINavigationController(rootViewController: self)
        viewController.present(navigationController, animated: true, completion: nil)
    }
    
    //Once presented, change screen to signIn or signUp
    func switchToAuthType(_ type: AuthType) {
        
        let destination: UIViewController
        
        if type == .signIn {
            title = "SIGN IN"
            destination = authStoryboard.instantiateViewController(withIdentifier: "signIn")
        } else {
            title = "SIGN UP"
            destination = authStoryboard.instantiateViewController(withIdentifier: "createAccount")
        }
        
        transition(to: destination)
    }
    
    //Root to the home screen
    static func rootToHome(isNewUser: Bool? = nil, with handle: String? = nil) {
        
        let homeStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let homeTabBarController = homeStoryboard.instantiateViewController(withIdentifier: "homeTabBarController") as! MainTabBarController
        homeTabBarController.handle = handle
        _ = homeTabBarController.view
        
        UIApplication.setRootView(with: homeTabBarController, animated: true) {
            guard let isNewUser = isNewUser else { return }
            if isNewUser {
                //Get home controller
                let homeController = homeTabBarController.children[0]
                Toast.show(message: "ACCOUNT CREATED", controller: homeController)
            }
        }
    }
    
    static func rootToAuth() {

        let homeStoryboard = UIStoryboard(name: "Auth", bundle: nil)
        let authBaseController = homeStoryboard.instantiateViewController(withIdentifier: "authBaseControllerId")
        UIApplication.setRootView(with: authBaseController, animated: true)
    }
}

extension AuthRouter {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(handleClose))
    }
    
    @objc func handleClose() {
        presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        view.endEditing(true)
    }
}
enum AuthType {
    case signIn
    case signUp
}
