//
//  Styles.swift
//  Bacon
//
//  Created by Robinson Paz Jesus on 9/4/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import UIKit

enum Color {
    
    case pageBackground
    
    case lightBackground
    case lineBackground
    
    
    case primary
    case secondary
    case accent
    
    //White
    case lightText
    //Dark Gray (never use pure black for text)
    case darkText
    
    //Avoid using this
    case custom(hexString: String, alpha: Double)
    
    //Alpha exists on the measurement controls
    func withAlpha(_ alpha: Double) -> UIColor {
        return self.value.withAlphaComponent(CGFloat(alpha))
    }
}

extension Color {
    
    var value: UIColor {
        var instanceColor = UIColor.clear
        
        switch self {
            
        //Primary app colors
        case .pageBackground:
            instanceColor = UIColor(hexString: "#f8f8f8")
        case .lightBackground:
            instanceColor = UIColor(hexString: "#eeeeee")
        case .lineBackground:
            instanceColor = UIColor(hexString: "#dddddd")
        case .primary:
            instanceColor = UIColor(red: 255/255, green: 73/255, blue: 0, alpha: 1)
        case .secondary:
            instanceColor = UIColor(red: 219/255, green: 27/255, blue: 64/255, alpha: 1)
        case .accent:
            instanceColor = UIColor(red: 219/255, green: 27/255, blue: 64/255, alpha: 1)
            
        //Text colors
        case .lightText:
            instanceColor = UIColor(hexString: "#999999")
        case .darkText:
            instanceColor = UIColor(hexString: "#222222")
            
            
        //Custom
        case .custom(let hexValue, let opacity):
            instanceColor = UIColor(hexString: hexValue).withAlphaComponent(CGFloat(opacity))
        }
        
        return instanceColor
    }
}

extension UIColor {
    /**
     Creates an UIColor from HEX String in "#363636" format
     - parameter hexString: HEX String in "#363636" format
     - returns: UIColor from HexString
     */
    convenience init(hexString: String) {
        
        let hexString: String = (hexString as NSString).trimmingCharacters(in: .whitespacesAndNewlines)
        let scanner          = Scanner(string: hexString as String)
        
        if hexString.hasPrefix("#") {
            scanner.scanLocation = 1
        }
        
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        
        self.init(red:red, green:green, blue:blue, alpha:1)
    }
}
