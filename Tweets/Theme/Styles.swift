
//  Styles.swift
//  Bacon
//
//  Created by Robinson Paz Jesus on 9/4/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import Foundation
import UIKit

//Standard Unit
let masterUnit: CGFloat = 8.0

enum Units {
    
    case semi
    case single
    case double
    case triple
    case quad
    case quint
    case sext
    case sept
    case oct
    
    var unit: CGFloat {
        switch self {
            
        case .semi:
            return masterUnit / 2   //4pt
        case .single:
            return masterUnit       //8pt
        case .double:
            return masterUnit * 2   //16pt
        case .triple:
            return masterUnit * 3   //24pt
        case .quad:
            return masterUnit * 4   //32pt
        case .quint:
            return masterUnit * 5   //40pt
        case .sext:
            return masterUnit * 6   //48pt
        case .sept:
            return masterUnit * 7   //56pt
        case .oct:
            return masterUnit * 8   //64pt
        }
    }
}

enum Spacing {
    
    //From edges of screens and views
    case edgeStandard
    case rowEdgeStandard
    case edgeGenerous
    
    
    //Between sections
    case betweenTitleAndSubtitle
    case betweenViewsSmall
    case betweenViewsMedium
    case betweenViewsLarge
    case betweenViewsXL
    case betweenButtons
    
    var value: CGFloat {
        switch self {
        case .edgeStandard:
            return Units.double.unit
        case .rowEdgeStandard:
            return 20
        case .edgeGenerous:
            return Units.quad.unit
        case .betweenTitleAndSubtitle:
            return Units.semi.unit
        case .betweenButtons:
            return Units.double.unit
        case .betweenViewsSmall:
            return Units.single.unit
        case .betweenViewsMedium:
            return Units.double.unit
        case .betweenViewsLarge:
            return Units.quad.unit
        case .betweenViewsXL:
            return Units.oct.unit
            
        }
    }
}

// Sizes Usage Examples
//let tableHeight = Sizes.tableHeight.value

enum Sizes {
    
    //Font sizes
    case h1
    case h2
    case body
    case h4
    case navItem
    case uppercaseKernValue
    
    
    //Corners
    case buttonCornerRadius
    case sheetCornerRadius
    case toastCornerRadius
    case sliderCornerRadius
    
    //Iconography
    case iconSize
    case smallIconSize
    
    //Heights
    case buttonHeight
    case measurementButtonHeight
    case lineBarHeight
    case sliderHeight
    case tableHeight
    case tableHeaderAndFooter
    case toastHeight
    
    //Widths
    case toastWidth
    
    var value: CGFloat {
        switch self {
            
        //Font Sizes
        case .h1:
            return 34.0
        case .h2:
            return 24.0
        case .body:
            return 14.0
        case .h4:
            return 12.0
        case .navItem:
            return 14.0
        case .uppercaseKernValue:
            return 0.4
            
        //Corner Radii
        case .buttonCornerRadius:
            return Units.single.unit
        case .sheetCornerRadius:
            return Units.quad.unit
        //Full rounding for toast and slider
        case .toastCornerRadius:
            return Sizes.toastHeight.value / 2
        case .sliderCornerRadius:
            return Sizes.measurementButtonHeight.value / 2
            
        //Iconography
        case .iconSize:
            return Units.triple.unit
        case .smallIconSize:
            return 12
            
        //Tables
        case .tableHeight:
            return Units.quint.unit * 2
        case .tableHeaderAndFooter:
            return Units.quad.unit
            
        //Control Heights
        case .buttonHeight:
            return Units.sext.unit
        case .sliderHeight:
            return Units.quint.unit
        case .lineBarHeight:
            return 2
        case .measurementButtonHeight:
            return Units.quint.unit
        case .toastHeight:
            return Units.sext.unit
            
        //Control Widths
        case .toastWidth:
            return 280
        }
    }
}


// Timing Usage Examples
//let timing    = AnimTiming.cheetah.time

enum AnimTiming {
    case cheetah
    //Ex: bottom sheet movement, toast feedback movement
    case rabbit
    case squirrel
    case sloth
    
    //Ex: fade away opacity
    case snail
    
    //Ex: time messages take to disapper
    case messageDelay
    
    var time: CGFloat {
        switch self {
        case .cheetah:
            return 0.2
        case .rabbit:
            return 0.4
        case .squirrel:
            return 0.6
        case .sloth:
            return 0.8
        case .snail:
            return 1.0
        case .messageDelay:
            return 6.0
        }
    }
}

// Font Usage Examples
//let h1    = Font.h1.instance
//let body  = Font.body.instance

enum Font {
    //Ex: Large Navbar Titles
    case h1
    
    //Ex: Date, Sheet title
    case h2
    
    //Ex: Table separator, control label
    case h4
    
    //Ex: body copy
    case bodyRegular
    
    //Ex: Button title
    case bodyMedium
    
    case bodySemibold
    
    //Ex: toast copy
    case bodyBold
    
    //Ex: nav title
    case navItem
    
    var style: UIFont {
        switch self {
        case .h1:
            return UIFont.systemFont(ofSize: Sizes.h1.value, weight: UIFont.Weight.heavy)
        case .h2:
            return UIFont.systemFont(ofSize: Sizes.h2.value, weight: UIFont.Weight.medium)
        case .h4:
            return UIFont.systemFont(ofSize: Sizes.h4.value, weight: UIFont.Weight.medium)
        case .bodyRegular:
            return UIFont.systemFont(ofSize: Sizes.body.value, weight: UIFont.Weight.regular)
        case .bodyMedium:
            return UIFont.systemFont(ofSize: Sizes.body.value, weight: UIFont.Weight.medium)
        case .bodySemibold:
            return UIFont.systemFont(ofSize: Sizes.body.value, weight: UIFont.Weight.semibold)
        case .bodyBold:
            return UIFont.systemFont(ofSize: Sizes.body.value, weight: UIFont.Weight.bold)
        case .navItem:
            return UIFont.systemFont(ofSize: Sizes.navItem.value, weight: UIFont.Weight.semibold)
            
        }
    }
}
