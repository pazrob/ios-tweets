//
//  File.swift
//  Tweets
//
//  Created by Robinson Paz Jesus on 9/6/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import UIKit

struct User: Codable {
    
    var firstName: String
    var lastName: String
    var handle: String?
    var photoUrl: String?
}
