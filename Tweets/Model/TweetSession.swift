//
//  TweetSession.swift
//  Tweets
//
//  Created by Robinson Paz Jesus on 10/14/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import UIKit
import AWSCognitoIdentityProvider

class TweetsSession {
    
    static var shared = TweetsSession()
    private init() {}
    
    var user: User!
    var photo = UIImage(named: "default_user")
    var idToken: String?
    var handle: String?
    
    func updateUser(user: User) {
        self.user = user
    }
    
    func updatePhoto(photo: UIImage) {
        self.photo = photo
    }
    
    func updateSession(session: AWSCognitoIdentityUserSession) {
        idToken = session.idToken?.tokenString
    }
    
}
