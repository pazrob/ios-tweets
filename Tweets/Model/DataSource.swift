//
//  DataSource.swift
//  Tweets
//
//  Created by Robinson Paz Jesus on 9/9/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import UIKit
import AVKit
import NVActivityIndicatorView

protocol DataSourceDelegate: class {
    func loadAnotherPage()
}

enum RowType {

    case feed(Status)
    case following(User)
    case follower(User)
    
    /// Cell ID of the cell type
    var identifier: String {
        switch self {
        case .feed:
            return FeedTableViewCell.identifier
        case .following, .follower:
            return UserTableViewCell.identifier
        }
    }
    
    func getStatus() -> Status? {
        switch self {
        case .feed(let status):
            return status
        default:
            return nil
        }
    }
    
    func getUser() -> User? {
        switch self {
        case .following(let user), .follower(let user):
            return user
        default:
            return nil
        }
    }
    
}

class DataSource: NSObject {
    
    private weak var delegate: DataSourceDelegate?
    weak var viewController: UIViewController?
    weak var tableView: UITableView?
    let loadMoreButton = UIButton(type: .system)
    var rows = [RowType]()
    var user: User?
    var showLoadMoreButton = true
    
    private let activityIndicator: NVActivityIndicatorView = {
        let indicator = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 45, height: 45))
        indicator.type = . ballPulse
        indicator.color = .white
        return indicator
    }()
    
    init(viewController: UIViewController, delegate: DataSourceDelegate? = nil) {
        self.viewController = viewController
        self.delegate = delegate
    }
    
    func setUpTableView(_ tableView: UITableView) {
        self.tableView = tableView
        tableView.register(FeedTableViewCell.self, forCellReuseIdentifier: FeedTableViewCell.identifier)
        tableView.register(UserTableViewCell.self, forCellReuseIdentifier: UserTableViewCell.identifier)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableView.automaticDimension
        tableView.sectionHeaderHeight = 0
        tableView.sectionFooterHeight = 0
        tableView.separatorStyle = .none
        tableView.backgroundColor = Color.pageBackground.value
        
        if showLoadMoreButton {
            loadMoreButton.setTitle("Load More", for: .normal)
            loadMoreButton.setTitleColor(.white, for: .normal)
            loadMoreButton.roundCorners(radius: 8)
            loadMoreButton.backgroundColor = Color.primary.value
            loadMoreButton.addTarget(self, action: #selector(handleAddMoreData(_:)), for: .touchUpInside)
            loadMoreButton.addSubview(activityIndicator)
            activityIndicator.anchorCenterSuperview()
            let buttonBase = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 80))
            buttonBase.addSubview(loadMoreButton)
            loadMoreButton.fillSuperview(padding: UIEdgeInsets(top: 18, left: 16, bottom: 8, right: 16))
            tableView.tableFooterView = buttonBase
        }
    }
    
    func setUpTableHeaderView(headerView: UIView) {
        let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
        var headerFrame = headerView.frame
        headerFrame.size.height = height
        headerView.frame = headerFrame
        tableView?.tableHeaderView = headerView
        tableView?.layoutIfNeeded()
        viewController?.view.layoutIfNeeded()
    }
    
    func updateDataFollowing(with users: [User]) {
        rows.removeAll()
        users.forEach({ rows.append(.following($0))})
        tableView?.reloadData()
    }
    
    func updateDataFollowers(with users: [User]) {
        rows.removeAll()
        users.forEach({ rows.append(.follower($0))})
        tableView?.reloadData()
    }
    
    func updateDataStory(with story: [Status]) {
        rows.removeAll()
        story.forEach({ rows.append(.feed($0))})
        tableView?.reloadData()
    }
    
    func appendRow(row: RowType) {
        rows.append(row)
        tableView?.reloadData()
    }
    
    func appendStatusPage(page: [Status]) {
        page.forEach({ rows.append(.feed($0)) })
        tableView?.reloadData()
    }
    
    func appendFollowingPage(page: [User]) {
        page.forEach({ rows.append(.following($0)) })
        tableView?.reloadData()
    }
    
    func appendFollowersPage(page: [User]) {
        page.forEach({ rows.append(.follower($0)) })
        tableView?.reloadData()
    }
    
    func reloadTable() {
        tableView?.reloadData()
    }
    
    @objc private func handleAddMoreData(_ sender: UIButton) {
        loadMoreButton.setTitle("", for: .normal)
        activityIndicator.startAnimating()
        delegate?.loadAnotherPage()
    }
    
    func updateButtonNormal() {
        loadMoreButton.setTitle("Load More", for: .normal)
        activityIndicator.stopAnimating()
    }
    
    func updateButtonEnd() {
        loadMoreButton.setTitle("Seems to be the end", for: .normal)
        activityIndicator.stopAnimating()
    }
}

extension DataSource: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        loadMoreButton.isHidden = rows.isEmpty
        return rows.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = rows[indexPath.item]
        guard let cell = tableView.dequeueReusableCell(withIdentifier: row.identifier) else { return UITableViewCell() }
        switch row {
        case .feed(let feed):
            let feedCell = cell as? FeedTableViewCell
            feedCell?.configure(with: feed, user: self.user)
            feedCell?.textContent.delegate = self
            feedCell?.delegate = self
        case .following(let user), .follower(let user):
            let userCell = cell as? UserTableViewCell
            userCell?.configure(with: user, userType: row)
            userCell?.delegate = self
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let navigationController = viewController?.navigationController
        let cell = tableView.cellForRow(at: indexPath)
        cell?.isSelected = false
        switch rows[indexPath.item] {
        case .feed(let status):
            if rows.count == 1 { return }
            let statusVC = FeedVC()
            statusVC.loadOneStatusOnly(with: status)
            navigationController?.pushViewController(statusVC, animated: true)
        case .following(let user), .follower(let user):
            let userVC = UserHomeVC(with: user)
            userVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(userVC, animated: true)
        }
    }
}

extension DataSource: UserTableViewCellDelegate {
    
    func didTapFollowButton(cell: UITableViewCell, button: UIButton) {
        if let indexPath = tableView?.indexPath(for: cell) {
            let row = rows[indexPath.row]
            print("The selection was: ", row.getUser()!)
        }
    }
}

extension DataSource: FeedTableViewCellDelegate {
    
    func tableViewCell(didSelect cell: UITableViewCell) {
        if let indexPath = tableView?.indexPath(for: cell) {
            let status = rows[indexPath.row].getStatus()
            let playerController = AVPlayerViewController()
            let player = AVPlayer(url: URL(string: status!.bodyUrl!)!)
            playerController.player = player
            playerController.showsPlaybackControls = true
            viewController?.present(playerController, animated: true, completion: nil)
        }
        
    }
}

extension DataSource: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        handleLink(viewController: viewController, URL: URL)
        return false
    }
}

extension UITextViewDelegate {
    func handleLink(viewController: UIViewController?, URL: URL) {
        let string = URL.absoluteString
        if string.hasPrefix("@") {
            viewController?.startLoading()
            let userNetwork = UserNetworkController()
            userNetwork.searchUser(handle: string)
                .done { user in
                    let userHomeVC = UserHomeVC(with: user)
                    userHomeVC.title = string
                    viewController?.navigationController?.pushViewController(userHomeVC, animated: true)
            }.catch { error in
                log.error(error)
                UIAlertController.presentError(viewController: viewController!, message: "User not found")
            }.finally {
                viewController?.stopAnimating()
            }
        } else if string.hasPrefix("#") {
            viewController?.startLoading()
            let statusNetwork = StatusNetworkController()
            statusNetwork.getStatuses(for: string)
                .done { statusListResponse in
                    if statusListResponse.items!.isEmpty {
                        UIAlertController.presentError(viewController: viewController!, message: "No status matches the hashtag 😓")
                    } else {
                        let feedVC = FeedVC()
                        feedVC.hideShowMoreButton()
                        feedVC.title = string
                        feedVC.updateData(statusList: statusListResponse.items!, lastEvaluatedKey: statusListResponse.lastEvaluatedKey?.sortKey)
                        viewController?.navigationController?.pushViewController(feedVC, animated: true)
                    }
                }.catch { error in
                log.error(error)
                UIAlertController.presentError(viewController: viewController!, message: "No status matches the hashtag 😓")
            }.finally {
                viewController?.stopAnimating()
            }
        } else {
            UIApplication.shared.open(URL)
        }
    }
}
