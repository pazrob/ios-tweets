//
//  Feed.swift
//  Tweets
//
//  Created by Robinson Paz Jesus on 9/4/19.
//  Copyright © 2019 Robinson Paz Jesus. All rights reserved.
//

import UIKit


/// Struct to read from statuses
struct Status: Codable {
    
    let id: String
    let text: String
    let handle: String
    let timestamp: Int?
    let fullName: String?
    let authorUrl: String?
    let bodyUrl: String?
    let viewURL: String?
    
    func getDate() -> String? {
        guard let timestamp = self.timestamp else { return nil }
        //Get the gap from the timestamp and present
        let gapFromPresent = Int(Date().timeIntervalSince1970) - timestamp
        
        //If text is older than a week
        if gapFromPresent > 604800 {
            
            let timestampDate = Date(timeIntervalSince1970: Double(timestamp))
            let dateFormater = DateFormatter()
            dateFormater.dateFormat = "MMM-d"
            return dateFormater.string(from: timestampDate)
        }
        
        //If text is older than a day
        if gapFromPresent > 86400 {
            
            let timestampDate = Date(timeIntervalSince1970: Double(timestamp))
            let dateFormater = DateFormatter()
            dateFormater.dateFormat = "E"
            return dateFormater.string(from: timestampDate)
        }
        
        //If text is younger than a minute
        //FIX: There is no update after 60 secs automaticly
        if gapFromPresent < 20 {
            return "Just Now"
        }
        
        //If text is between 60 sec and a day
        let timestampDate = Date(timeIntervalSince1970: Double(timestamp))
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "h:mm a"
        return dateFormater.string(from: timestampDate)
        
    }
}
